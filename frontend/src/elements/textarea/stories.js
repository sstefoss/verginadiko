import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withKnobs, select, text} from '@storybook/addon-knobs/react';

import Textarea from './index';
const sizes = ['md'];

const actionProps = {
  onChange: action('onChange')
};

storiesOf('Textarea', module)
  .addDecorator(withKnobs)
  .add('with default value', () =>
    <div>
      <Textarea
        {...actionProps}
        defaultValue={text('Default value', 'Default textarea value')}
        placeholder={text('Placeholder', undefined)}
        size={select('Size', sizes, 'md')}
      />
    </div>
  )
  .add('with placeholder', () =>
    <div>
      <Textarea
        {...actionProps}
        defaultValue={text('Default value', '')}
        placeholder={text('Placeholder', 'Textarea placeholder')}
        size={select('Size', sizes, 'md')}
      />
    </div>
  );
