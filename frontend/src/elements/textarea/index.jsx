import TextArea from 'react-textarea-autosize';
import './style.pcss';

export default class Textarea extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    defaultValue: PropTypes.string,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    size: PropTypes.string,
    minRows: PropTypes.number,
    onBlur: PropTypes.func,
    focusOnMount: PropTypes.bool
  };

  static defaultProps = {
    size: 'md',
    defaultValue: '',
    onChange: () => {},
    onBlur: () => {},
    placeholder: '',
    minRows: 1,
    focusOnMount: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      empty: props.defaultValue.length === 0
    };
  }

  componentDidMount() {
    const { focusOnMount } = this.props;
    if (focusOnMount) {
      this.textarea.focus();
    }
  }

  get value() {
    return this.textarea.value;
  }

  onChange = e => {
    this.setState({ empty: e.target.value.length === 0 });
    this.props.onChange(e);
  };

  render() {
    const { defaultValue, className, size, placeholder, minRows } = this.props;
    const { empty } = this.state;
    // define classNames
    const COMPONENT = 'c-textarea';
    const wrapperCN = classNames(COMPONENT, className, `${COMPONENT}--${size}`);
    const textAreaCN = classNames(`${COMPONENT}__textarea`, {
      [`${COMPONENT}--is-empty`]: empty
    });

    return (
      <div className={wrapperCN}>
        <TextArea
          inputRef={textarea => (this.textarea = textarea)}
          minRows={minRows}
          maxRows={5}
          onChange={this.onChange}
          onBlur={this.props.onBlur}
          className={textAreaCN}
          defaultValue={defaultValue}
          placeholder={placeholder}
        />
      </div>
    );
  }
}
