import Icon from './index';

describe('<Icon />', () => {

  it('should render', () => {
    const comp = renderer.create(
      <Icon
        icon='beer'
        className='test-class'
      />
    );
    expect(comp).toMatchSnapshot();
  });

  it('should set correct class names', () => {
    const wrapper = shallow(<Icon icon='beer' />);
    expect(wrapper.hasClass('fa')).toEqual(true);
    expect(wrapper.hasClass('fa-beer')).toEqual(true);
  });

  it('should trigger onClick event', () => {
    const onClick = jest.fn();
    const wrapper = shallow(
      <Icon
        icon='beer'
        onClick={onClick}
      />
    );
    wrapper.simulate('click');
    expect(onClick).toHaveBeenCalled();
  });

  it('should set "fa-spin" class when spinning', () => {
    const wrapper = shallow(
      <Icon
        icon='beer'
        isSpinning
      />
    );
    expect(wrapper.hasClass('fa-spin')).toEqual(true);
  });
});
