import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withKnobs, text, boolean} from '@storybook/addon-knobs/react';
import Icon from './index';

const actionProps = {
  onClick: action('onClick')
};

storiesOf('Icon', module)
  .addDecorator(withKnobs)
  .add('with fa icon', () =>
    <div>
      <Icon
        {...actionProps}
        icon={text('Icon', 'check')}
        isSpinning={boolean('Spinning', false)}
      />
    </div>
  )
  .add('with spinning icon', () =>
    <div>
      <Icon
        {...actionProps}
        icon={text('Icon', 'check')}
        isSpinning={boolean('Spinning', true)}
      />
    </div>
  );
