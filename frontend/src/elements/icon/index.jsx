export default class Icon extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    icon: PropTypes.string.isRequired,
    isSpinning: PropTypes.bool,
  };

  static defaultProps = {
    className: undefined,
    isSpinning: false
  };

  render() {
    const { icon, className, isSpinning } = this.props;
    const cn = classNames('fa', className, `fa-${icon}`, {
      'fa-spin': isSpinning
    });
    return <i {...this.props} className={cn} />;
  }
}
