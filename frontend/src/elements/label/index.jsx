const COMPONENT = 'c-label';
import './style';

export default class Label extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    size: PropTypes.string,
  };

  static defaultProps = {
    type: 'primary',
    size: 'md'
  };

  render() {
    const { type, value, className, size } = this.props;
    const cn = classNames(
      COMPONENT,
      `${COMPONENT}--${type}`,
      `${COMPONENT}--${size}`,
      className
    );
    return <span className={cn}>{value}</span>;
  }
}
