import Icon from '../icon/index';
import './style';

export default class Checkbox extends React.PureComponent {
  static propTypes = {
    alignLabel: PropTypes.string,
    checked: PropTypes.bool.isRequired,
    className: PropTypes.string,
    inactive: PropTypes.bool,
    label: PropTypes.string,
    icon: PropTypes.string,
    onClick: PropTypes.func,
    validated: PropTypes.bool,
    radio: PropTypes.bool,
    size: PropTypes.oneOf(['sm', 'md']),
    type: PropTypes.oneOf(['primary', 'transparent'])
  };

  static defaultProps = {
    alignLabel: 'right',
    checked: false,
    inactive: false,
    label: undefined,
    icon: 'check',
    validated: true,
    radio: false,
    onClick: () => {},
    size: 'md',
    type: 'primary'
  };

  onClick = e => {
    const { onClick, inactive } = this.props;
    if (inactive) {
      e.preventDefault();
      e.stopPropagation();
      return;
    }
    onClick(e);
  };

  render() {
    const {
      alignLabel,
      checked,
      className,
      inactive,
      label,
      icon,
      radio,
      size,
      validated,
      type
    } = this.props;

    // component classes
    const COMPONENT = 'c-checkbox';
    const componentClassName = classNames(COMPONENT, className, {
      [`${COMPONENT}--${type}`]: true,
      [`${COMPONENT}--${size}`]: true,
      [`${COMPONENT}--checked`]: checked,
      [`${COMPONENT}--rounded`]: radio,
      [`${COMPONENT}--inactive`]: inactive,
      [`${COMPONENT}--invalid`]: !validated,
      [`${COMPONENT}--label-first`]: alignLabel && alignLabel === 'left'
    });

    // prepare input
    const inputAttributes = {
      readOnly: true,
      ref: ref => (this.input = ref),
      type: radio ? 'radio' : 'checkbox',
      onClick: this.onClick,
      checked: checked,
      className: `${COMPONENT}__input`
    };
    const input = <input {...inputAttributes} />;

    // prepare input label
    const inputLabel = label && (
      <span onClick={this.onClick} className={`${COMPONENT}__label`}>
        {label}
      </span>
    );

    const tickboxWrapper = (
      <span className={`${COMPONENT}__tickbox`}>
        <Icon icon={radio ? 'circle' : icon} className={`${COMPONENT}__tick`} />
      </span>
    );

    const component =
      alignLabel === 'left' ? (
        <div className={componentClassName}>
          {inputLabel}
          {input}
          {tickboxWrapper}
        </div>
      ) : (
        <div className={componentClassName}>
          {tickboxWrapper}
          {input}
          {inputLabel}
        </div>
      );
    return component;
  }
}
