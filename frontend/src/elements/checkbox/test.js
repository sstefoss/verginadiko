import Checkbox from './index';
import Icon from '../icon/index';

describe('<Checkbox />', () => {
  it('should render', () => {
    const comp = renderer.create(
      <Checkbox
        className='test-class'
        type='primary'
        size='sm'
        inactive
        radio
        checked
        label='Test'
      />
    );
    expect(comp).toMatchSnapshot();
  });

  /**
   * Test props
   */

  it('should render input, label and a tickbox', () => {
    const wrapper = shallow(<Checkbox label='test label' />);
    expect(wrapper.find('input').length).toEqual(1);
    expect(wrapper.find(Icon).length).toEqual(1);
    expect(wrapper.find('.c-checkbox__label').length).toEqual(1);
    expect(wrapper.find('.c-checkbox__tickbox').length).toEqual(1);
    expect(wrapper.find('.c-checkbox__tick').length).toEqual(1);
  });

  it('should render label before checkbox', () => {
    const wrapper = shallow(<Checkbox label='left label' alignLabel='left' />);
    expect(wrapper.hasClass('c-checkbox--label-first')).toEqual(true);
    expect(wrapper.childAt(0).hasClass('c-checkbox__label')).toEqual(true);
  });

  it('should set classes', () => {
    const wrapper = shallow(
      <Checkbox
        className='test-class'
        size='sm'
        inactive
        validated={false}
        radio
        checked
        label='Test'
      />
    );
    expect(wrapper.hasClass('test-class')).toEqual(true);
    expect(wrapper.hasClass('c-checkbox--primary')).toEqual(true);
    expect(wrapper.hasClass('c-checkbox--rounded')).toEqual(true);
    expect(wrapper.hasClass('c-checkbox--inactive')).toEqual(true);
    expect(wrapper.hasClass('c-checkbox--invalid')).toEqual(true);
    expect(wrapper.hasClass('c-checkbox--checked')).toEqual(true);
    expect(wrapper.hasClass('c-checkbox--sm')).toEqual(true);
    expect(wrapper.hasClass('c-checkbox--primary')).toEqual(true);
  });

  it('should set correct attributes to input', () => {
    const wrapper = shallow(<Checkbox checked radio />);
    const input = wrapper.find('input');
    expect(input.props().checked).toEqual(true);
    expect(input.props().type).toEqual('radio');
  });

  it('should set icon passed as prop', () => {
    const wrapper = shallow(<Checkbox icon='minus' />);
    expect(wrapper.find(Icon).props().icon).toEqual('minus');
  });

  /**
   * Test events
   */

  it('should trigger onClick event when clicking on <input /> or label', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<Checkbox label='test' onClick={onClick} />);
    wrapper.find('input').simulate('click');
    wrapper.find('span.c-checkbox__label').simulate('click', {
      preventDefault: () => {},
      stopPropagation: () => {}
    });
    expect(onClick).toHaveBeenCalledTimes(2);
  });

  it('should not trigger onClick event when inactive is passed as prop', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<Checkbox inactive onClick={onClick} />);
    wrapper.find('input').simulate('click', {
      preventDefault: () => {},
      stopPropagation: () => {}
    });
    expect(onClick).not.toHaveBeenCalled();
  });
});
