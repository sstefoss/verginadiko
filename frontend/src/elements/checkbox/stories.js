import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, select, boolean } from '@storybook/addon-knobs/react';

import Checkbox from './index';

const actionProps = {
  onClick: action('onClick')
};

const sizes = ['sm', 'md'];
const alignLabel = ['left', 'right'];
const types = ['primary', 'transparent'];

storiesOf('Checkbox', module)
  .addDecorator(withKnobs)
  .add('with label', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'left')}
        checked={boolean('Checked', true)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        label={text('Label', 'Checkbox label')}
        icon={text('Icon', 'check')}
        radio={boolean('Radio', false)}
        size={select('Size', sizes, 'md')}
        tooltip={text('Tooltip', undefined)}
        type={select('Type', types, 'primary')}
      />
    </div>
  ))
  .add('without label', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'left')}
        checked={boolean('Checked', true)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        label={text('Label', undefined)}
        icon={text('Icon', 'check')}
        radio={boolean('Radio', false)}
        size={select('Size', sizes, 'md')}
        tooltip={text('Tooltip', undefined)}
        type={select('Type', types, 'primary')}
      />
    </div>
  ))
  .add('with types', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'left')}
        checked={boolean('Checked', true)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        label={text('Label', undefined)}
        icon={text('Icon', 'check')}
        radio={boolean('Radio', false)}
        size={select('Size', sizes, 'md')}
        tooltip={text('Tooltip', undefined)}
        type={select('Type', types, 'transparent')}
      />
    </div>
  ))
  .add('inactive', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'left')}
        checked={boolean('Checked', true)}
        inactive={boolean('Inactive', true)}
        validated={boolean('Validated', true)}
        label={text('Label', undefined)}
        icon={text('Icon', 'check')}
        radio={boolean('Radio', false)}
        size={select('Size', sizes, 'md')}
        tooltip={text('Tooltip', undefined)}
        type={select('Type', types, 'primary')}
      />
    </div>
  ))
  .add('unchecked', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'left')}
        checked={boolean('Checked', false)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        label={text('Label', undefined)}
        icon={text('Icon', 'check')}
        radio={boolean('Radio', false)}
        size={select('Size', sizes, 'md')}
        tooltip={text('Tooltip', undefined)}
        type={select('Type', types, 'primary')}
      />
    </div>
  ))
  .add('with sizes', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'left')}
        checked={boolean('Checked', true)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        label={text('Label', undefined)}
        icon={text('Icon', 'check')}
        radio={boolean('Radio', false)}
        size={select('Size', sizes, 'sm')}
        tooltip={text('Tooltip', undefined)}
        type={select('Type', types, 'primary')}
      />
    </div>
  ))
  .add('with tooltip', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'left')}
        checked={boolean('Checked', true)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        label={text('Label', undefined)}
        icon={text('Icon', 'check')}
        radio={boolean('Radio', false)}
        size={select('Size', sizes, 'md')}
        tooltip={text('Tooltip', 'Tooltip text')}
        type={select('Type', types, 'primary')}
      />
    </div>
  ))
  .add('with minus icon', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'right')}
        checked={boolean('Checked', true)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        label={text('Label', 'Checkbox label')}
        icon={text('Icon', 'minus')}
        radio={boolean('Radio', false)}
        size={select('Size', sizes, 'md')}
        tooltip={text('Tooltip', undefined)}
        type={select('Type', types, 'primary')}
      />
    </div>
  ))
  .add('as radio', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'right')}
        checked={boolean('Checked', true)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        label={text('Label', 'Radio label')}
        icon={text('Icon', 'check')}
        radio={boolean('Radio', true)}
        size={select('Size', sizes, 'md')}
        tooltip={text('Tooltip', undefined)}
        type={select('Type', types, 'primary')}
      />
    </div>
  ))
  .add('with right label alignment', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'right')}
        checked={boolean('Checked', true)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        label={text('Label', 'Checkbox label')}
        icon={text('Icon', 'check')}
        radio={boolean('Radio', false)}
        size={select('Size', sizes, 'md')}
        tooltip={text('Tooltip', undefined)}
        type={select('Type', types, 'primary')}
      />
    </div>
  ))
  .add('invalid', () => (
    <div>
      <Checkbox
        {...actionProps}
        alignLabel={select('Align label', alignLabel, 'right')}
        checked={boolean('Checked', true)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', false)}
        label={text('Label', 'Checkbox label')}
        icon={text('Icon', 'check')}
        radio={boolean('Radio', false)}
        size={select('Size', sizes, 'md')}
        tooltip={text('Tooltip', undefined)}
        type={select('Type', types, 'primary')}
      />
    </div>
  ));
