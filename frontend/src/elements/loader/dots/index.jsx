import './style';
const COMPONENT = 'c-spinner';

export default class Spinner extends React.PureComponent {

  static propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    visible: PropTypes.bool
  }

  static defaultProps = {
    visible: false
  }

  render() {
    const {visible, className, type} = this.props;
    const cn = classNames(COMPONENT, className, {
      [`${COMPONENT}--${type}`]: type
    });
    return (
      <div style={{display: visible ? 'block' : 'none'}} className={cn}>
        <svg viewBox='0 0 48 32' width='48' height='32' fill='#6ec3c7'>
          <circle transform='translate(12 0)' cx='0' cy='16' r='0'>
            <animate
              attributeName='r' values='3; 5; 3; 3' dur='1.2s' repeatCount='indefinite' begin='0'
              keyTimes='0;0.2;0.7;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8' calcMode='spline' />
          </circle>
          <circle transform='translate(24 0)' cx='0' cy='16' r='0'>
            <animate
              attributeName='r' values='3; 5; 3; 3' dur='1.2s' repeatCount='indefinite' begin='0.15'
              keyTimes='0;0.2;0.7;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8' calcMode='spline' />
          </circle>
          <circle transform='translate(36 0)' cx='0' cy='16' r='0'>
            <animate
              attributeName='r' values='3; 5; 3; 3' dur='1.2s' repeatCount='indefinite' begin='0.3'
              keyTimes='0;0.2;0.7;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8' calcMode='spline' />
          </circle>
        </svg>
      </div>
    );
  }
}
