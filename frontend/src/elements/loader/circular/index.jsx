import './style';
export default class Loader extends React.PureComponent {

  static propTypes = {
    className: PropTypes.string,
    alwaysVisible: PropTypes.bool,
    strokeWidth: PropTypes.string,
    visible: PropTypes.bool,
    type: PropTypes.string
  }

  render() {
    const {alwaysVisible, strokeWidth, visible, type, className} = this.props;
    const cn = classNames(className, 'loader', {
      [`loader--${type}`]: typeof type !== 'undefined'
    });
    const _visible = (alwaysVisible || visible) ? true : false;
    const _strokeWidth = (strokeWidth) ? strokeWidth : '5';

    return (
      <div style={{display: (_visible) ? 'block' : 'none'}} className={cn}>
        <svg className='circular' viewBox='25 25 50 50'>
          <circle className='path' cx='50' cy='50' r='20' fill='none' strokeWidth={_strokeWidth} strokeMiterlimit='10' />
        </svg>
      </div>
    );
  }
}
