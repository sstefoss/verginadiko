/**
 * @fileoverview Loader component.
 */

import CircularLoader from './circular';
import DotsLoader from './dots';

export {
  CircularLoader,
  DotsLoader
};
