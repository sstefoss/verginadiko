import PerfectScrollbar from 'react-perfect-scrollbar';
import filter from 'lodash/filter';
import unionWith from 'lodash/unionWith';
import isEqual from 'lodash/isEqual';
import Input from '../input/index';
import DotsLoader from '../loader/dots/index';
import './style';

const COMPONENT = 'c-autocomplete';

export default class Autocomplete extends React.PureComponent {
  static propTypes = {
    apiSuggestions: PropTypes.bool,
    className: PropTypes.string,
    clearOnSelection: PropTypes.bool,
    clearOnBlur: PropTypes.bool,
    data: PropTypes.object.isRequired,
    defaultValue: PropTypes.string,
    filteringFunc: PropTypes.func,
    inactive: PropTypes.bool,
    inputClassName: PropTypes.string,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onSelection: PropTypes.func,
    placeholder: PropTypes.string,
    reqThreshold: PropTypes.number,
    selectOnBlur: PropTypes.bool,
    suggestionsTop: PropTypes.number,
    size: PropTypes.string,
    validated: PropTypes.bool
  };

  /**
   * @constructor
   * State:
   *   @param {Array} visibleSuggestions Visible suggestions.
   *   @param {boolean} focused Whether autocomplete is focused.
   */
  constructor(props) {
    super(props);
    const { defaultValue, data } = props;
    // to avoid async tests set this to 0
    this.reqThreshold = props.reqThreshold ? props.reqThreshold : 750;
    // this is used onBlur event, to check if blur was triggered after pressing
    // enter or not
    this.enterPressedEvt = false;
    this.testPrivateFunc = (func, params) => this[func](params);
    this.offset = 0;
    this.state = {
      focused: false,
      isFetching: false,
      initialDataFetched: false,
      value: defaultValue ? defaultValue : '',
      preselected: null,
      async: typeof data.fetchAction !== 'undefined'
    };
  }

  static defaultProps = {
    size: 'md',
    filteringFunc: items => items
  };

  componentDidMount() {
    this.mounted = true;
    this.asyncFetch('').then(() => {
      this.updateState({ initialDataFetched: true });
      if (this.delayPromise) {
        this.delayPromise.then(() => {
          const value = this.state.value;
          if (value !== '') {
            this.asyncFetch(value);
          }
        });
      }
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  scrollEnded = () => {
    // this is used to limit 'scrollend' events
    if (
      typeof this.scrollEndedPromise !== 'undefined' &&
      this.scrollEndedPromise.isPending()
    ) {
      return;
    }

    this.scrollEndedPromise = Promise.delay(100).then(() => {
      const inputVal = this.inputRef.input.value;
      return this.asyncFetch(inputVal, true);
    });
  };

  updateState = (newState, cb) => {
    if (this.mounted) {
      this.setState(newState, cb);
    }
  };

  onSelection = (val, e) => {
    const inputVal = this.inputRef.input.value;
    if (this.props.onSelection) {
      this.props.onSelection(val, inputVal, e);
    }
  };

  onFocus = () => {
    const state = {
      focused: true,
      preselected: 0
    };
    // change focused state
    this.updateState(state);

    if (this.props.onFocus) {
      this.props.onFocus();
    }
  };

  onBlur = e => {
    const { onBlur, selectOnBlur, clearOnBlur } = this.props;
    const { preselected } = this.state;
    // change focused state
    this.updateState({
      focused: false,
      preselected: null
    });

    // if selectOnBlur is set, call onSelection with selected element
    if (selectOnBlur) {
      const inputVal = this.inputRef.input.value;
      const filtered = this.filterSuggestions(inputVal);
      const selection =
        this.enterPressedEvt && filtered[preselected]
          ? filtered[preselected]
          : inputVal;
      this.onSelection(selection, e);
    }

    if (clearOnBlur) {
      this.updateState({ value: '' });
    }

    // call props' onBlur
    if (onBlur) {
      const inputVal = this.inputRef.input.value;
      const filtered = this.filterSuggestions(inputVal);
      const selection =
        this.enterPressedEvt && filtered[preselected]
          ? filtered[preselected]
          : inputVal;
      onBlur(selection);
    }

    this.enterPressedEvt = false;
  };

  onChange = e => {
    const { onChange } = this.props;
    this.offset = 0;
    const value = this.inputRef.input.value;
    // fetch async data
    this.asyncFetch(value);
    // filter suggestions
    const suggestions = this.filterSuggestions(value);
    // update state
    this.updateState({
      value,
      preselected: suggestions.length > 0 ? 0 : null
    });

    // call props' onChange
    if (onChange) {
      onChange(e);
    }
  };

  onKeyUp = e => {
    if (e.keyCode === 13) {
      // enter
      this.enterPressed(e);
    } else if (e.keyCode === 40) {
      // down
      this.downPressed();
    } else if (e.keyCode === 38) {
      // up
      this.upPressed();
    } else if (e.keyCode === 27) {
      // esc
      this.escPressed();
    }
  };

  onMouseDown = (e, el) => {
    const { clearOnSelection } = this.props;
    const { displayValue } = this.props.data;
    const state = { preselected: null };
    if (clearOnSelection) {
      // if clearOnSeletion, clear value
      state.value = '';
    } else {
      const value = displayValue(el);
      state.value = value ? value : '';
    }
    this.updateState(state);
    this.onSelection(el, e);
  };

  onMouseEnter = index => {
    this.updatePreselected(index);
  };

  enterPressed = e => {
    // enter pressed
    const { data, clearOnSelection, selectOnBlur } = this.props;
    const { preselected, focused } = this.state;
    const filtered = this.filterSuggestions(this.inputRef.input.value);
    const selection =
      preselected !== null ? filtered[preselected] : this.inputRef.input.value;
    this.enterPressedEvt = true;

    this.updateState({
      value: clearOnSelection ? '' : data.displayValue(selection)
    });

    if (focused) {
      this.inputRef.input.blur();
    }

    if (!selectOnBlur) {
      // no need to call 'onSelection' if selectOnBlur is true
      this.onSelection(selection, e);
    }
  };

  escPressed = () => {
    // blur input
    this.inputRef.input.blur();
  };

  upPressed = () => {
    const { data } = this.props;
    const { source } = data;
    const { preselected } = this.state;
    const index = preselected - 1 < 0 ? source.length - 1 : preselected - 1;
    // change value of preselected index
    this.updatePreselected(index);
    // check whether to scroll or not
    if (typeof source[index] !== 'undefined') {
      const $el = this.scrollBarWrapperRef.querySelector(
        `div[data-key='${data.key(source[index])}']`
      );
      this.checkScroll($el);
    }
  };

  downPressed = () => {
    const { data } = this.props;
    const { source } = data;
    const { preselected } = this.state;
    const index = preselected + 1 === source.length ? 0 : preselected + 1;
    // change value of preselected index
    this.updatePreselected(index);
    // check whether to scroll or not
    if (typeof source[index] !== 'undefined') {
      const $el = this.scrollBarWrapperRef.querySelector(
        `div[data-key='${data.key(source[index])}']`
      );
      this.checkScroll($el);
    }
  };

  updatePreselected = preselected => {
    this.disallowAsync = true;
    this.updateState({ preselected }, () => {
      this.disallowAsync = false;
    });
  };

  checkScroll = $el => {
    if (!$el || $el.length === 0) {
      return;
    }
    // check if dropdown needs to scroll down/up to show hidden elements
    if ($el.offsetTop > this.scrollable.offsetHeight - 80) {
      this.containerRef.scrollTop = $el.offsetTop + 40;
    } else if (
      $el.offsetTop <= this.scrollable.offsetHeight - 80 ||
      $el.offsetTop <= 0
    ) {
      this.containerRef.scrollTop = 0;
    }
  };

  asyncFetch = (val, isPagination) => {
    const { data } = this.props;
    if (!data.fetchAction || this.offset === -1) {
      // no fetch action is defined
      // or offset has reached the limit
      return Promise.resolve();
    }
    // if promise is pending, return
    if (this.delayPromise && this.delayPromise.isPending()) {
      this.delayPromise.cancel();
    }
    // set delayPromose
    this.delayPromise = Promise.delay(this.reqThreshold);
    return this.delayPromise
      .then(() => {
        let param;
        if (val) {
          param = val;
        } else if (this.inputRef && this.inputRef.value) {
          param = this.inputRef.value;
        } else {
          param = '';
        }
        // then fetch data
        this.updateState({ isFetching: true });
        if (this.offset === -1) {
          return Promise.resolve([]);
        }
        return data.fetchAction(param.trim(), this.offset);
      })
      .then(res => this.dataFetched(res, isPagination));
  };

  dataFetched = (res, isPagination) => {
    if (
      !this.inputRef ||
      (this.inputRef && typeof this.inputRef.value === 'undefined')
    ) {
      this.updateState({ isFetching: false });
      return Promise.resolve();
    }

    const suggestions = this.filterSuggestions(this.inputRef.value, res);
    this.offset = res && res.length > 0 ? this.offset + 10 : -1;
    this.updateState({
      isFetching: false,
      preselected:
        isPagination || suggestions.length === 0 ? this.state.preselected : 0
    });
    this.scrollBarRef.updateScroll();
    return Promise.resolve();
  };

  filterSuggestions = (val, res = this.props.data.source) => {
    const { source } = this.props.data;
    const { apiSuggestions, filteringFunc } = this.props;
    // if there is no source
    if (!source) {
      return [];
    }
    // if input is empty
    if (val.length === 0) {
      const returnData = filteringFunc ? filteringFunc(source) : source;
      return returnData;
    }
    // filter matched strings
    const filtered = filter(source, item => this.matchDisplayValue(item, val));
    let returnData = apiSuggestions
      ? unionWith(res, filtered, isEqual)
      : filtered;
    if (filteringFunc) {
      returnData = filteringFunc(returnData);
    }
    return returnData;
  };

  matchDisplayValue = (item, val) => {
    const { displayValue } = this.props.data;
    if (
      escape(displayValue(item).toLowerCase()).match(escape(val.toLowerCase()))
    ) {
      return item;
    }
    return null;
  };

  shouldRenderSpinner = () => {
    const { isFetching, initialDataFetched } = this.state;
    const { data } = this.props;

    // return true
    // fetchAction is defined AND
    // offset is not -1 AND
    // when request is in progress OR
    // if initial data have not been fetched
    return (
      data.fetchAction &&
      this.offset !== -1 &&
      (!initialDataFetched || isFetching)
    );
  };

  renderSuggestions = () => {
    const { data } = this.props;
    const { source } = data;

    const { preselected } = this.state;
    if (!this.inputRef) {
      return [];
    }
    const value = this.inputRef.input.value;
    return this.filterSuggestions(value.trim(), source).map((el, index) => {
      // className for preselected items
      const itemCN = classNames(`${COMPONENT}__suggestion`, {
        [`${COMPONENT}__suggestion--preselected`]:
          (preselected === null && index === 0) ||
          (preselected !== null && index === preselected)
      });

      return (
        <div
          onMouseDown={e => this.onMouseDown(e, el, index)}
          onMouseEnter={() => this.onMouseEnter(index)}
          className={itemCN}
          data-key={data.key(el)}
          key={data.key(el)}
        >
          {data.template(el)}
        </div>
      );
    });
  };

  render() {
    const {
      className,
      inputClassName,
      size,
      validated,
      placeholder,
      inactive,
      suggestionsTop,
      data
    } = this.props;
    const { focused, value, isFetching } = this.state;
    const { source } = data;
    if (!source) {
      return null;
    }
    const val = !this.inputRef ? '' : this.inputRef.input.value;
    const suggestions = this.filterSuggestions(val, source);
    const isSpinnerVisible = this.shouldRenderSpinner();

    // component class
    const wrapperCN = classNames(COMPONENT, className, {
      [`${COMPONENT}--is-empty`]: suggestions.length === 0,
      [`${COMPONENT}--${size}`]: typeof size !== 'undefined',
      [`${COMPONENT}--is-focused`]:
        (focused && suggestions.length !== 0) || (focused && isFetching),
      [`${COMPONENT}--spinner-visible`]: isSpinnerVisible
    });
    // input and suggestions
    const inputCN = classNames(`${COMPONENT}__input`, inputClassName);
    const suggestionsCN = classNames(`${COMPONENT}__suggestions`, {
      [`${COMPONENT}__suggestions--is-invisible`]:
        !focused || (suggestions.length === 0 && !isFetching),
      [`${COMPONENT}__suggestions--is-visible`]:
        (focused && suggestions.length !== 0) || isFetching
    });
    // spinner class
    const spinner = (
      <div className={`${COMPONENT}__spinner`}>
        <DotsLoader type='gray' visible />
      </div>
    );
    const suggestionsStyle = suggestionsTop ? { top: suggestionsTop } : {};
    let suggestionsHeight = suggestions.length * 48;
    if (suggestions.length === 0) {
      suggestionsHeight = 48;
    } else if (suggestions.length > 4) {
      suggestionsHeight = 192;
    }

    if (isSpinnerVisible) {
      suggestionsHeight += 54;
    }

    return (
      <div className={wrapperCN}>
        <div className={inputCN}>
          <Input
            size={size}
            value={value}
            ref={ref => (this.inputRef = ref)}
            className={inputClassName}
            validated={validated}
            placeholder={placeholder}
            inactive={inactive}
            onChange={this.onChange}
            onFocus={this.onFocus}
            onKeyUp={this.onKeyUp}
            onBlur={this.onBlur}
            actions={[
              {
                key: 'icon',
                icon: 'caret-down',
                onMouseDown: e => {
                  if (this.state.focused) {
                    this.inputRef.input.blur();
                  } else {
                    this.inputRef.input.focus();
                  }
                  e.preventDefault();
                  e.stopPropagation();
                  return null;
                }
              }
            ]}
          />
        </div>
        <div
          ref={ref => (this.scrollable = ref)}
          style={suggestionsStyle}
          className={suggestionsCN}
        >
          <div
            className={`${COMPONENT}__scrollbar-wrapper`}
            style={{
              position: 'relative',
              width: '100%',
              height: suggestionsHeight
            }}
            ref={ref => (this.scrollBarWrapperRef = ref)}
          >
            <PerfectScrollbar
              containerRef={ref => (this.containerRef = ref)}
              ref={ref => (this.scrollBarRef = ref)}
              option={{ minScrollbarLength: 40 }}
              onYReachEnd={this.scrollEnded}
            >
              {this.renderSuggestions()}
              {isSpinnerVisible && spinner}
            </PerfectScrollbar>
          </div>
        </div>
      </div>
    );
  }
}
