import {storiesOf} from '@storybook/react';
import {withKnobs, text} from '@storybook/addon-knobs/react';

import Breadcrumb from './index';

storiesOf('Breadcrumb', module)
  .addDecorator(withKnobs)
  .add('default', () =>
    <div>
      <Breadcrumb
        name={text('Value', 'Active')}
      />
    </div>
  )
  .add('with parents', () =>
    <div>
      <Breadcrumb
        name={text('Value', 'Active')}
        parents={[{
          name: 'Parent 1',
          link: '/1'
        }, {
          name: 'Parent 2',
          link: '/1/2'
        }]}
      />
    </div>
  );
