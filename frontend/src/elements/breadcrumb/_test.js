import Breadcrumb from './index';

describe('<Breadcrumb />', () => {
  it('should render', () => {
    const comp = renderer.create(
      <Breadcrumb
        name='test'
        parents={[{
          name: 'Admin',
          link: '/admin/'
        }, {
          name: 'Teams',
          link: '/admin/teams/'
        }]} />
    );
    expect(comp).toMatchSnapshot();
  });

  it('should set name', () => {
    const wrapper = shallow(
      <Breadcrumb name='test' />
    );
    expect(wrapper.find('.c-breadcrumb__item--is-active').text()).toEqual('test');
  });

  it('should set children', () => {
    const wrapper = shallow(
      <Breadcrumb
        name='test'
        parents={[{
          name: 'Admin',
          link: '/admin/'
        }, {
          name: 'Teams',
          link: '/admin/teams/'
        }]} />
    );
    expect(wrapper.find('ol').find('li').length).toEqual(4);
  });
});
