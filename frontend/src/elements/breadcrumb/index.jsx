import { Link } from 'react-router-dom';
import './style';

const COMPONENT = 'c-breadcrumb';

export default class Breadcrumb extends React.PureComponent {
  static propTypes = {
    name: PropTypes.string.isRequired,
    parents: PropTypes.arrayOf(
      PropTypes.shape({
        link: PropTypes.string,
        name: PropTypes.string
      })
    )
  };

  static defaultProps = {
    name: '',
    parents: []
  };

  render() {
    const { name, parents } = this.props;
    return (
      <div className={`${COMPONENT}__wrapper`}>
        <ol className={COMPONENT}>
          <li>
            <Link to='/'>Dashboard</Link>
          </li>
          {parents &&
            parents.map(parent => (
              <li key={Math.random()}>
                <Link to={parent.link}>{parent.name}</Link>
              </li>
            ))}
          <li className={`${COMPONENT}__item--is-active`}>{name}</li>
        </ol>
      </div>
    );
  }
}
