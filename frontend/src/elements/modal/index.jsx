import ModalWrapper from './wrapper';
import Icon from '../icon/index';
import './style';

export { ModalWrapper };

const COMPONENT = 'c-modal';

export default class Modal extends React.PureComponent {
  static propTypes = {
    blocking: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    closeIcon: PropTypes.bool,
    size: PropTypes.string,
    onHide: PropTypes.func,
  };

  static defaultProps = {
    blocking: false,
    className: null,
    size: 'md',
    closeIcon: false,
    onHide: () => {}
  };

  render() {
    const {
      className,
      children,
      closeIcon,
      size,
      onHide,
      blocking
    } = this.props;
    const componentCN = classNames(
      className,
      COMPONENT,
      `${COMPONENT}--${size}`
    );

    return (
      <div className={componentCN}>
        {closeIcon && (
          <div
            onClick={blocking ? null : onHide}
            className={`${COMPONENT}__close`}
          >
            <button className={`${COMPONENT}__close-icon`}>
              <Icon icon='times' />
            </button>
            Close
          </div>
        )}
        <div className={`${COMPONENT}__body`}>
          {children}
        </div>
        <div className={`${COMPONENT}__footer`} />
      </div>
    );
  }
}
