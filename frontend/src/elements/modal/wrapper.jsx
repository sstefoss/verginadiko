import Transition from 'react-transition-group/Transition';
import './style';

const COMPONENT = 'c-modal-wrapper';
const BODY_CLASS = 'is-modal-visible';

export default class ModalWrapper extends React.PureComponent {
  static propTypes = {
    isBlocking: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    onClose: PropTypes.func,
    visible: PropTypes.bool
  };

  static defaultProps = {
    visible: false,
    children: null,
    onClose: () => {},
    isBlocking: false
  };

  componentDidMount() {
    const { visible } = this.props;
    if (visible) {
      document.body.classList.toggle(BODY_CLASS, true);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.visible !== this.props.visible) {
      document.body.classList.toggle(BODY_CLASS, nextProps.visible);
    }
  }

  componentWillUnmount() {
    document.body.classList.toggle(BODY_CLASS, false);
  }

  render() {
    const { onClose, isBlocking, visible, children } = this.props;
    return (
      <Transition
        unmountOnExit
        mountOnEnter
        in={visible}
        timeout={{
          enter: 120,
          exit: 100
        }}
      >
        {state => (
          <div className={`${COMPONENT} ${COMPONENT}--animation-${state}`}>
            <div
              onClick={isBlocking ? null : onClose}
              className={`${COMPONENT}__overlay`}
            />
            <div className={`${COMPONENT}__content`}>
              {children}
            </div>
          </div>
        )}
      </Transition>
    );
  }
}
