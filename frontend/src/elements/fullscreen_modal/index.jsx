import { CSSTransition, TransitionGroup } from 'react-transition-group';
import './style';
import Icon from '../icon/index';
const COMPONENT = 'c-fullscreen-modal';

export default class FullscreenModal extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    hasBack: PropTypes.bool,
    hasClose: PropTypes.bool,
    onBack: PropTypes.func,
    onHide: PropTypes.func
  };

  static defaultProps = {
    className: null,
    children: null,
    onHide: () => {},
    onBack: () => {},
    hasClose: true,
    hasBack: false
  };

  state = {
    visible: false,
    willHide: false
  };

  componentWillUnmount() {
    document.body.classList.toggle('is-modal-open', false);
  }

  show = () => {
    document.body.classList.toggle('is-modal-open', true);
    this.setState({ visible: true });
  };

  hide = () => {
    const { onHide } = this.props;
    document.body.classList.toggle('is-modal-open', false);
    this.setState({ visible: false });
    onHide();
  };

  render() {
    const { className, children, onBack, hasBack, hasClose } = this.props;
    const { visible } = this.state;
    const cn = classNames(COMPONENT, className);
    const elements = visible
      ? [
        <CSSTransition
          key='FullscreenModal__wrapper'
          classNames={`${COMPONENT}__fade-in`}
          timeout={150}
          >
          <div className={cn}>
            <div>
              {hasBack && (
              <div className={`${COMPONENT}__back`} onClick={onBack}>
                <Icon icon='arrow-left' />
              </div>
                )}
              {hasClose && (
              <div className={`${COMPONENT}__close`} onClick={this.hide}>
                <Icon icon='close' />
              </div>
                )}
              <div className={`${COMPONENT}__content`}>{children}</div>
            </div>
          </div>
        </CSSTransition>
        ]
      : [];
    return <TransitionGroup>{elements}</TransitionGroup>;
  }
}
