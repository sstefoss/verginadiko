import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withKnobs, boolean} from '@storybook/addon-knobs/react';

import FullscreenModal from './index';

const actionProps = {
  onBack: action('onBack'),
  onHide: action('onHide'),
};

// TODO: should change this
let modal = null;
let modal1 = null;
let modal2 = null;

storiesOf('FullscreenModal', module)
  .addDecorator(withKnobs)
  .add('default', () =>
    <div>
      <div
        onClick={() => {
        modal.show();
      }}>Click me</div>
      <FullscreenModal
        {...actionProps}
        ref={ref => modal = ref}
        hasClose={boolean('Has close', false)}
        hasBack={boolean('Has back', false)}
      >
        <h1>Full scren modal content</h1>
      </FullscreenModal>
    </div>
  )
  .add('with back icon', () =>
    <div>
      <div
        onClick={() => {
        modal1.show();
      }}>Click me</div>
      <FullscreenModal
        {...actionProps}
        ref={ref => modal1 = ref}
        hasClose={boolean('Has close', false)}
        hasBack={boolean('Has back', true)}
      >
        <h1>Full scren modal content</h1>
      </FullscreenModal>
    </div>
  )
  .add('with close icon', () =>
    <div>
      <div
        onClick={() => {
        modal2.show();
      }}>Click me</div>
      <FullscreenModal
        {...actionProps}
        ref={ref => modal2 = ref}
        hasClose={boolean('Has close', true)}
        hasBack={boolean('Has back', false)}
      >
        <h1>Full scren modal content</h1>
      </FullscreenModal>
    </div>
  );
