import Dropdown from './index';

const TIMESPAN_DATA = {
  every_week: 'Weekly',
  every_2_weeks: '2 week period',
  every_3_weeks: '3 week period',
  every_4_weeks: '4 week period',
  first_week_of_month: 'First week of each month',
  last_week_of_month: 'Last week of each month'
};

const data = Object.keys(TIMESPAN_DATA).map(o => ({
  data: TIMESPAN_DATA[o],
  value: o,
  onClick: () => {}
}));

describe('<Dropdown />', () => {
  /**
   * Test props
   */
  it('should set classNames', () => {
    const wrapper = shallow(
      <Dropdown inactive validated={false} className='test-class' data={data} />
    );
    expect(wrapper.find('.c-dropdown').length).toEqual(1);
    expect(wrapper.find('.c-dropdown--has-error').length).toEqual(1);
    expect(wrapper.find('.c-dropdown--inactive').length).toEqual(1);
    expect(wrapper.find('.test-class').length).toEqual(1);
  });

  it('should render dropdown with tooltip', () => {
    const wrapper = shallow(
      <Dropdown tooltip={<div className='tooltip-comp'>Tooltip</div>} />
    );
    expect(wrapper.find('.c-dropdown__tooltip').length).toEqual(1);
    expect(wrapper.find('.tooltip-comp').length).toEqual(1);
  });

  it('should render dropdown with label passed as prop', () => {
    const wrapper = shallow(<Dropdown label='Label' data={data} />);
    expect(wrapper.find('.c-dropdown__label').text()).toEqual('Label');
  });

  it('should render dropdown with label component passed as prop', () => {
    const wrapper = shallow(
      <Dropdown
        data={data}
        labelComp={<div className='custom-label'>Custom label</div>}
      />
    );
    expect(wrapper.find('.custom-label').text()).toEqual('Custom label');
  });

  /**
   * Test events
   */
  it('should open menu onClick', () => {
    const wrapper = shallow(<Dropdown data={data} />);
    const button = wrapper.find('.c-dropdown__button');
    button.simulate('click');
    expect(wrapper.state().open).toEqual(true);
    button.simulate('click');
    expect(wrapper.state().open).toEqual(false);
  });

  it('should not open menu onClick when inactive is passed as prop', () => {
    const wrapper = shallow(<Dropdown data={data} inactive />);
    const button = wrapper.find('.c-dropdown__button');
    button.simulate('click');
    expect(wrapper.state().open).toEqual(false);
  });

  it('should not open menu onClick when no data are passed as prop', () => {
    const wrapper = shallow(<Dropdown />);
    const button = wrapper.find('.c-dropdown__button');
    button.simulate('click');
    expect(wrapper.state().open).toEqual(false);
  });

  it('should open menu onClick when no data but tooltip are passed as prop', () => {
    const wrapper = shallow(
      <Dropdown tooltip={<div className='tooltip-comp'>Tooltip</div>} />
    );
    const button = wrapper.find('.c-dropdown__button');
    button.simulate('click');
    expect(wrapper.state().open).toEqual(true);
  });

  it("should call item's onClick function when triggered", () => {
    const onClick = jest.fn();
    const _data = Object.keys(TIMESPAN_DATA).map(o => ({
      data: TIMESPAN_DATA[o],
      value: o,
      onClick
    }));
    const wrapper = shallow(<Dropdown data={_data} />);
    const item = wrapper.find('.c-dropdown__scroll-content').childAt(0);
    item.simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
