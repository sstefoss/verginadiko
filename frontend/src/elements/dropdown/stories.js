import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean } from '@storybook/addon-knobs/react';
import Dropdown from './index';

const TIMESPAN_DATA = {
  every_week: 'Weekly',
  every_2_weeks: '2 week period',
  every_3_weeks: '3 week period',
  every_4_weeks: '4 week period',
  first_week_of_month: 'First week of each month',
  last_week_of_month: 'Last week of each month'
};

const data = Object.keys(TIMESPAN_DATA).map(o => ({
  data: TIMESPAN_DATA[o],
  value: o,
  onClick: () => {}
}));

storiesOf('Dropdown', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <div>
      <Dropdown
        label={text('Label', 'Default')}
        validated={boolean('Validated', true)}
        inactive={boolean('Inactive', false)}
        data={data}
      />
    </div>
  ))
  .add('invalid', () => (
    <div>
      <Dropdown
        label={text('Label', 'Default')}
        validated={boolean('Validated', false)}
        inactive={boolean('Inactive', false)}
        data={data}
      />
    </div>
  ))
  .add('inactive', () => (
    <div>
      <Dropdown
        label={text('Label', 'Default')}
        validated={boolean('Validated', false)}
        inactive={boolean('Inactive', true)}
        data={data}
      />
    </div>
  ));
