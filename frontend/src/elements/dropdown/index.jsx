import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';
import ClickOutside from 'react-click-outside';
import StaticPopover from '../static_popover/index';
import './style';

const COMPONENT = 'c-dropdown';

export default class Dropdown extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    data: PropTypes.array,
    inactive: PropTypes.bool,
    label: PropTypes.string,
    labelComp: PropTypes.node,
    size: PropTypes.string,
    validated: PropTypes.bool,
    tooltip: PropTypes.node
  };

  static defaultProps = {
    data: [],
    inactive: false,
    label: '',
    size: 'md',
    validated: true,
    tooltip: undefined
  };

  state = {
    open: false
  };

  onClick = item => {
    const { open } = this.state;
    const { data, inactive, tooltip } = this.props;
    if ((data.length === 0 && !tooltip) || inactive) {
      // there are no data or dropdown is inactive
      return;
    }
    if (open) {
      this.setState({ open: false });
    } else {
      this.setState({ open: true });
    }
    if (item && item.onClick) {
      item.onClick();
    }
  };

  onClickOutside = () => {
    this.setState({ open: false });
  };

  setOpenState = open => {
    this.setState({ open });
  };

  render() {
    const {
      className,
      label,
      data,
      validated,
      size,
      inactive,
      labelComp,
      tooltip
    } = this.props;
    const { open } = this.state;
    const componentCN = classNames(
      COMPONENT,
      className,
      `${COMPONENT}--${size}`,
      {
        [`${COMPONENT}--has-error`]: !validated,
        [`${COMPONENT}--inactive`]: inactive,
        [`${COMPONENT}--is-open`]: open,
        [`${COMPONENT}--has-tooltip`]: typeof tooltip !== 'undefined'
      }
    );

    const contentHeight = data.length > 5 ? 200 : data.length * 48;

    return (
      <ClickOutside onClickOutside={this.onClickOutside}>
        <div ref={ref => (this.parent = ref)} className={componentCN}>
          <button
            className={`${COMPONENT}__button`}
            type='button'
            onFocusOut={this.onFocusOut}
            onClick={this.onClick}
          >
            {labelComp && <span style={{ width: '100%' }}>{labelComp}</span>}
            {!labelComp && (
              <span className={`${COMPONENT}__label`}>{label}</span>
            )}
            <span className={`${COMPONENT}__caret`} />
          </button>
          {tooltip ? (
            <div className={`${COMPONENT}__tooltip`}>
              <StaticPopover>{tooltip}</StaticPopover>
            </div>
          ) : (
            <div className={`${COMPONENT}__menu`} role='menu'>
              <div
                className={`${COMPONENT}__scroll-wrapper`}
                ref={ref => (this.scrollable = ref)}
              >
                <PerfectScrollbar>
                  <div
                    style={{
                      width: '100%',
                      position: 'relative',
                      height: contentHeight
                    }}
                    className={`${COMPONENT}__scroll-content`}
                  >
                    {data.map(item => {
                      const props = {
                        onClick: () => this.onClick(item),
                        key: Math.random()
                      };
                      if (typeof item.component !== 'undefined') {
                        return <div {...props}>{item.component}</div>;
                      }
                      return <div {...props}>{item.data}</div>;
                    })}
                  </div>
                </PerfectScrollbar>
              </div>
            </div>
          )}
        </div>
      </ClickOutside>
    );
  }
}

class DropdownSeparator extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
  };

  static defaultProps = {
    className: null,
    children: null
  };

  render() {
    const { className, children } = this.props;
    const cn = classNames(`${COMPONENT}__separator`, className, {
      [`${COMPONENT}__separator--empty`]: !children
    });
    return (
      <div key={Math.random()} className={cn}>
        {children}
      </div>
    );
  }
}

export { DropdownSeparator };
