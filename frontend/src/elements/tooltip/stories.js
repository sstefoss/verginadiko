import {storiesOf} from '@storybook/react';
import {withKnobs, select, text} from '@storybook/addon-knobs/react';

const types = ['primary', 'danger'];
const positions = ['top', 'bottom'];
import Tooltip from './index';

storiesOf('Tooltip', module)
  .addDecorator(withKnobs)
  .add('with text', () =>
    <div style={{padding: '100px'}}>
      <Tooltip
        value={text('Value', 'Hello tooltip')}
        type={select('Type', types, 'primary')}
        html={text('Html', undefined)}
        position={select('Position', positions, 'top')}>
        Element
      </Tooltip>
    </div>
  )
  .add('with html', () =>
    <div style={{padding: '100px'}}>
      <Tooltip
        value={text('Value', 'Hello tooltip')}
        html={text('Html', '<a>link</a> tooltip')}
        type={select('Type', types, 'primary')}
        position={select('Position', positions, 'top')}>
        Element
      </Tooltip>
    </div>
  )
  .add('with positions', () =>
    <div style={{padding: '100px'}}>
      <Tooltip
        value={text('Value', 'Hello tooltip')}
        html={text('Html', undefined)}
        type={select('Type', types, 'primary')}
        position={select('Position', positions, 'bottom')}>
        Element
      </Tooltip>
    </div>
  )
  .add('with types', () =>
    <div style={{padding: '100px'}}>
      <Tooltip
        value={text('Value', 'Hello tooltip')}
        html={text('Html', undefined)}
        type={select('Type', types, 'danger')}
        position={select('Position', positions, 'top')}>
        Element
      </Tooltip>
    </div>
  );
