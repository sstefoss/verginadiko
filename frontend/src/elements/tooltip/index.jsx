const COMPONENT = 'c-tooltip';
import './style';

export default class Tooltip extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    value: PropTypes.string,
    html: PropTypes.string,
    type: PropTypes.oneOf(['primary', 'danger']),
    children: PropTypes.node,
    position: PropTypes.string
  };

  static defaultProps = {
    position: 'top',
    html: null,
    type: 'primary'
  };

  state = {
    visible: false
  };

  show = () => {
    this.setState({ visible: true });
  }

  hide = () => {
    this.setState({ visible: false });
  }

  render() {
    const { className, html, value, children, position, type } = this.props;
    const { visible } = this.state;
    const cn = classNames(className, COMPONENT, {
      [`${COMPONENT}--${position}`]: true,
      [`${COMPONENT}--${type}`]: true,
      [`${COMPONENT}--is-visible`]: visible
    });
    if (!value && !html) {
      return children;
    }
    const contentCN = `${COMPONENT}__inner`;
    return (
      <div
        className={`${COMPONENT}__wrapper`}
        onMouseEnter={this.show}
        onMouseLeave={this.hide}
      >
        {children}
        <div onMouseLeave={this.hide} className={cn}>
          {html ? (
            <div
              className={contentCN}
              dangerouslySetInnerHTML={{ __html: html }}
            />
          ) : (
            <div className={contentCN}>{value}</div>
          )}
          <div
            className={classNames(
              `${COMPONENT}__arrow`,
              `${COMPONENT}__arrow--${position}`
            )}
          />
        </div>
      </div>
    );
  }
}
