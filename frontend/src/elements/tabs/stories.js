import {storiesOf} from '@storybook/react';
import {withKnobs, boolean} from '@storybook/addon-knobs/react';

import Tab from './tab';
import Tabs from './tabs';

storiesOf('Tabs', module)
  .addDecorator(withKnobs)
  .add('default', () =>
    <div>
      <Tabs>
        <Tab name='Tab 01'>
          <h3>Tab 01</h3>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </Tab>
        <Tab
          name='Tab 02'
          disabled={boolean('Disabled', false)}>
          <h3>Tab 02</h3>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </Tab>
        <Tab
          name='Tab 03'
          disabled={boolean('Disabled', false)}>
          <h3>Tab 03</h3>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </Tab>
      </Tabs>
    </div>
  )
  .add('with disabled tab', () =>
    <div>
      <Tabs>
        <Tab name='Tab 01' >
          <h3>Tab 01</h3>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </Tab>
        <Tab
          name='Tab 02'
          disabled={boolean('Disabled', true)}>
          <h3>Tab 02</h3>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </Tab>
        <Tab
          name='Tab 03'
          disabled={boolean('Disabled', true)}>
          <h3>Tab 03</h3>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </Tab>
      </Tabs>
    </div>
  );
