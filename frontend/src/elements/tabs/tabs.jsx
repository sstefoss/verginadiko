// import { scrollTo } from 'utils/extra';
import MediaQuery from 'react-responsive';

const COMPONENT = 'c-tabs';
import './style';

export default class Tabs extends React.PureComponent {

  static propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    fullWidth: PropTypes.bool,
    onTabChange: PropTypes.func,
    noPadding: PropTypes.bool,
  }

  static defaultProps = {
    onTabChange: () => {},
    noPadding: false,
    fullWidth: false,
    className: null,
    children: undefined
  }

  state = {
    active: 0
  }

  onClick(index, disabled, id, isMobile = false) {
    const {onTabChange} = this.props;
    if (disabled) {
      return;
    }
    if (isMobile && this.state.active === index) {
      index = null;
    }

    if (index !== null) {
      window.location.hash = `#${id}`;
    }
    this.setState({active: index}, () => {
      if (index && isMobile) {
        const section = document.getElementById(id);
        if (typeof section.offsetTop !== 'undefined') {
          // scrollTo(section.offsetTop - 95, 50);
        }
      }
    });
    onTabChange(index);
  }

  /**
   * Shows tab.
   *
   * @public
   * @param {string} tab ('basic', 'schedule', 'members')
   */
  show(tab) {
    let index = 0;
    if (tab === 'questions') {
      index = 1;
    } else if (tab === 'members') {
      index = 2;
    } else if (tab === 'schedule') {
      index = 3;
    } else if (tab === 'advanced') {
      index = 4;
    }
    this.setState({active: index});
  }

  showIndex(index) {
    this.setState({active: index});
  }

  render() {
    const {
      children,
      fullWidth,
      className,
      noPadding
    } = this.props;
    const {active} = this.state;
    const tabNames = [];
    const disabled = [];
    const tabIds = [];
    children.forEach(child => {
      if (child) {
        tabNames.push(child.props.name);
        disabled.push(child.props.disabled);
        tabIds.push(child.props.id);
      }
    });

    const childrenWithProps = React.Children.map(children, (child, index) => {
      if (child) {
        return React.cloneElement(child, {
          id: tabIds[index],
          active: active === index,
          disabled: disabled[index],
          onClick: () => this.onClick(index, disabled[index], tabIds[index], true)
        });
      }
      return null;
    });

    const parentClassName = classNames(COMPONENT, className, {
      [`${COMPONENT}--full-width`]: fullWidth,
      [`${COMPONENT}--no-padding`]: noPadding
    });

    return (
      <div className={parentClassName}>
        <MediaQuery query='(min-width: 768px)'>
          <div className={`${COMPONENT}__head`}>
            <div className={`${COMPONENT}__tabs`}>
              {tabNames.map((name, index) => {
                const tabCN = `${COMPONENT}__tab`;
                const tabClassName = classNames(tabCN, {
                  [`${tabCN}--is-active`]: index === active,
                  [`${tabCN}--is-disabled`]: disabled[index]
                });
                return (
                  <div
                    key={index}
                    onClick={() => this.onClick(index, disabled[index], tabIds[index], false)}
                    className={tabClassName}>{name}</div>
                );
              })}
            </div>
          </div>
        </MediaQuery>
        <div className={`${COMPONENT}__body`}>
          {childrenWithProps}
        </div>
      </div>
    );
  }
}
