const COMPONENT = 'c-tab';

import Icon from '../icon/index';
import MediaQuery from 'react-responsive';

export default class Tab extends React.PureComponent {

  static propTypes = {
    active: PropTypes.bool,
    disabled: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    onClick: PropTypes.func,
  }

  render() {
    const {children, active, className, disabled, name, onClick, id} = this.props;
    const cn = classNames(COMPONENT, className, {
      [`${COMPONENT}--is-active`]: active,
      [`${COMPONENT}--is-disabled`]: disabled
    });
    const headCN = classNames(`${COMPONENT}__head`);
    const bodyCN = classNames(`${COMPONENT}__body`);
    return (
      <div className={cn}>
        <MediaQuery query='(min-width: 768px)'>
          {children}
        </MediaQuery>
        <MediaQuery query='(max-width: 767px)'>
          <div id={id} onClick={onClick} className={headCN}>
            <h2>{name}</h2>
            <Icon icon={`chevron-${active ? 'up' : 'down'}`} />
          </div>
          <div className={bodyCN}>
            {children}
          </div>
        </MediaQuery>
      </div>
    );
  }
}
