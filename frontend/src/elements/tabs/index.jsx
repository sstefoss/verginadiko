/**
 * @fileoverview Implements tab component.
 */

import Tabs from './tabs';
import Tab from './tab';

export {
  Tab,
  Tabs
};
