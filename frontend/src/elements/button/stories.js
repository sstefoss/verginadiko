import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, boolean, select, text } from '@storybook/addon-knobs/react';

import Button from './index';

const types = ['primary', 'warning', 'subtle', 'danger', 'link'];
const sizes = ['md', 'lg'];

const actionProps = {
  onClick: action('onClick'),
  onMouseUp: action('onMouseUp')
};

storiesOf('Button', module)
  .addDecorator(withKnobs)
  .add('with text', () => (
    <div>
      <Button
        {...actionProps}
        type={select('Type', types, 'primary')}
        size={select('Sizes', sizes, 'md')}
        value={text('Text', 'Primary button')}
        disabled={boolean('Disabled', false)}
        fullWidth={boolean('Full width', false)}
        showLoader={boolean('Loader', false)}
        icon={text('FA icon', null)}
      />
    </div>
  ))
  .add('disabled', () => (
    <div>
      <Button
        {...actionProps}
        type={select('Type', types, 'primary')}
        size={select('Sizes', sizes, 'md')}
        value={text('Text', 'Primary button')}
        disabled={boolean('Disabled', true)}
        fullWidth={boolean('Full width', false)}
        showLoader={boolean('Loader', false)}
        icon={text('FA icon', null)}
      />
    </div>
  ))
  .add('with icon', () => (
    <div>
      <Button
        {...actionProps}
        type={select('Type', types, 'primary')}
        size={select('Sizes', sizes, 'md')}
        value={text('Text', 'Primary button')}
        disabled={boolean('Disabled', false)}
        fullWidth={boolean('Full width', false)}
        showLoader={boolean('Loader', false)}
        icon={text('FA icon', 'check')}
      />
    </div>
  ))
  .add('with types', () => (
    <div>
      <Button
        {...actionProps}
        type={select('Type', types, 'warning')}
        size={select('Sizes', sizes, 'md')}
        value={text('Text', 'Primary button')}
        disabled={boolean('Disabled', false)}
        fullWidth={boolean('Full width', false)}
        showLoader={boolean('Loader', false)}
        icon={text('FA icon', null)}
      />
    </div>
  ))
  .add('with full width', () => (
    <div>
      <Button
        {...actionProps}
        type={select('Type', types, 'primary')}
        size={select('Sizes', sizes, 'md')}
        value={text('Text', 'Primary button')}
        disabled={boolean('Disabled', false)}
        fullWidth={boolean('Full width', true)}
        showLoader={boolean('Loader', false)}
        icon={text('FA icon', null)}
      />
    </div>
  ))
  .add('with loader', () => (
    <div>
      <Button
        {...actionProps}
        type={select('Type', types, 'primary')}
        size={select('Sizes', sizes, 'md')}
        value={text('Text', 'Primary button')}
        disabled={boolean('Disabled', false)}
        fullWidth={boolean('Full width', false)}
        showLoader={boolean('Loader', true)}
        icon={text('FA icon', null)}
      />
    </div>
  ))
  .add('with sizes', () => (
    <div>
      <Button
        {...actionProps}
        type={select('Type', types, 'primary')}
        size={select('Sizes', sizes, 'lg')}
        value={text('Text', 'Large Buttoni')}
        disabled={boolean('Disabled', false)}
        fullWidth={boolean('Full width', false)}
        showLoader={boolean('Loader', false)}
        icon={text('FA icon', null)}
      />
    </div>
  ));
