import Button from './index';
import Icon from '../icon/index';
import DotsLoader from '../loader/dots/index';

describe('<Button />', () => {
  it('should match snapshot', () => {
    const comp = renderer.create(
      <Button value='test' icon='beer' type='danger' fullWidth size='lg' />
    );
    expect(comp).toMatchSnapshot();
  });

  /**
   * Test props
   */
  it('should set classes', () => {
    const wrapper = shallow(
      <Button
        value='test'
        className='test-class'
        fullWidth
        size='md'
        type='primary'
        showLoader
      />
    );
    expect(wrapper.hasClass('test-class')).toEqual(true);
    expect(wrapper.hasClass('c-button--is-loading')).toEqual(true);
    expect(wrapper.hasClass('c-button--full-width')).toEqual(true);
    expect(wrapper.hasClass('c-button--primary')).toEqual(true);
    expect(wrapper.hasClass('c-button--md')).toEqual(true);
  });

  it('should set correct value', () => {
    const wrapper = shallow(<Button value='test' />);
    expect(wrapper.find('.c-button__label').text()).toEqual('test');
  });

  it('should render inactive button', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<Button value='test' disabled onClick={onClick} />);
    expect(wrapper.hasClass('c-button--is-disabled')).toEqual(true);
    wrapper.simulate('click');
    expect(onClick).not.toHaveBeenCalled();
  });

  it('should render icon', () => {
    const wrapper = shallow(<Button value='test' icon='beer' />);
    expect(wrapper.find(Icon).length).toEqual(1);
    expect(wrapper.find(Icon).props().icon).toEqual('beer');
  });

  it('should show loader', () => {
    const wrapper = shallow(<Button value='test' showLoader />);
    expect(wrapper.find(DotsLoader).length).toEqual(1);
  });

  it('should align icon on the right', () => {
    const wrapper = shallow(
      <Button value='test' alignIcon='right' icon='beer' />
    );
    const label = wrapper.find('.c-button').childAt(0);
    expect(label.hasClass('c-button__label')).toEqual(true);
    const icon = wrapper.find('.c-button').childAt(1);
    expect(icon.hasClass('c-button__icon')).toEqual(true);
  });

  it('should align icon on the left', () => {
    const wrapper = shallow(<Button value='test' icon='beer' />);
    const label = wrapper.find('.c-button').childAt(1);
    expect(label.hasClass('c-button__label')).toEqual(true);
    const icon = wrapper.find('.c-button').childAt(0);
    expect(icon.hasClass('c-button__icon')).toEqual(true);
  });

  /**
   * Test events
   */

  it('should trigger onClick event', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<Button value='test' onClick={onClick} />);
    wrapper.simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
