import { DotsLoader } from '../loader/index';
import Icon from '../icon/index';
const COMPONENT = 'c-button';
import './style';

export default class Button extends React.PureComponent {
  static propTypes = {
    size: PropTypes.oneOf(['md', 'lg']),
    value: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['primary', 'warning', 'subtle', 'danger', 'link']),
    icon: PropTypes.string,
    className: PropTypes.string,
    alignIcon: PropTypes.string,
    fullWidth: PropTypes.bool,
    disabled: PropTypes.bool,
    showLoader: PropTypes.bool,
    onClick: PropTypes.func.isRequired
  };

  static defaultProps = {
    alignIcon: 'left',
    onClick: () => {},
    size: 'md',
    type: 'primary',
    fullWidth: false,
    disabled: false,
    className: undefined,
    showLoader: false,
    icon: null
  };

  render() {
    const {
      className,
      type,
      showLoader,
      icon,
      value,
      size,
      alignIcon,
      fullWidth,
      disabled
    } = this.props;

    const buttonCN = classNames(
      className,
      COMPONENT,
      `${COMPONENT}--${type}`,
      `${COMPONENT}--${size}`,
      {
        [`${COMPONENT}--is-loading`]: showLoader,
        [`${COMPONENT}--full-width`]: fullWidth,
        [`${COMPONENT}--is-disabled`]: disabled
      }
    );

    const labelComp = <span className={`${COMPONENT}__label`}>{value}</span>;
    const iconComp = icon && (
      <Icon className={`${COMPONENT}__icon`} icon={icon} size={size} />
    );
    const loaderComp = (
      <DotsLoader
        type={type}
        className={`${COMPONENT}__loader`}
        visible={showLoader}
      />
    );

    return (
      <button
        className={buttonCN}
        onClick={disabled ? null : this.props.onClick}
      >
        {alignIcon === 'right' ? (
          <React.Fragment>
            {labelComp}
            {iconComp}
            {loaderComp}
          </React.Fragment>
        ) : (
          <React.Fragment>
            {iconComp}
            {labelComp}
            {loaderComp}
          </React.Fragment>
        )}
      </button>
    );
  }
}
