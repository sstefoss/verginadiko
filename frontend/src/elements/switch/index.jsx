import './style';
const COMPONENT = 'c-switch';
const ON = 'On';
const OFF = 'Off';

export default class Switch extends React.Component {
  static propTypes = {
    defaultChecked: PropTypes.bool,
    className: PropTypes.string,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func
  };

  static defaultProps = {
    className: undefined,
    checked: null,
    defaultChecked: false,
    disabled: false,
    onChange: () => {}
  };

  constructor(props) {
    super(props);
    this.state = {
      checked:
        props.checked !== null
          ? props.checked
          : props.defaultChecked
    };
  }

  onClick = () => {
    const { onChange, disabled } = this.props;
    if (disabled) {
      return;
    }
    const val = !this.state.checked;
    this.setState({ checked: val });
    onChange(val);
  };

  render() {
    const { className, disabled, checked } = this.props;
    const value = typeof checked !== 'undefined' ? checked : this.state.checked;
    const cn = classNames(COMPONENT, className, {
      [`${COMPONENT}--is-disabled`]: disabled,
      [`${COMPONENT}--is-checked`]: value
    });
    return (
      <span className={cn} onClick={this.onClick}>
        <span className={`${COMPONENT}__inner`}>{value ? ON : OFF}</span>
      </span>
    );
  }
}
