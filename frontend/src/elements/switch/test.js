import Switch from './index';

describe('<Switch />', () => {
  it('should match snapshot', () => {
    const comp = renderer.create(
      <Switch disabled checked={false} className='test-class' />
    );
    expect(comp).toMatchSnapshot();
  });

  /**
   * Test props
   */
  it('should set classNames', () => {
    const wrapper = shallow(<Switch checked disabled className='test-class' />);
    expect(wrapper.hasClass('c-switch')).toEqual(true);
    expect(wrapper.hasClass('c-switch--is-checked')).toEqual(true);
    expect(wrapper.hasClass('c-switch--is-disabled')).toEqual(true);
    expect(wrapper.hasClass('test-class')).toEqual(true);
  });

  it('should set state.checked value', () => {
    const wrapper = shallow(<Switch defaultChecked={false} />);
    expect(wrapper.state().checked).toEqual(false);
  });

  /**
   * Test events
   */
  it('should trigger onChange event', () => {
    const onChange = jest.fn();
    const wrapper = shallow(<Switch onChange={onChange} />);
    wrapper.simulate('click');
    expect(onChange).toHaveBeenCalled();
  });

  it('should not trigger onChange event when inactive', () => {
    const onChange = jest.fn();
    const wrapper = shallow(<Switch disabled onChange={onChange} />);
    wrapper.simulate('click');
    expect(onChange).not.toHaveBeenCalled();
  });
});
