import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withKnobs, boolean} from '@storybook/addon-knobs/react';
import Switch from './index';

const actionProps = {
  onChange: action('onChange')
};

storiesOf('Switch', module)
  .addDecorator(withKnobs)
  .add('checked', () =>
    <div>
      <Switch
        {...actionProps}
        checked={boolean('Checked', true)}
        disabled={boolean('Disabled', false)}
      />
    </div>
  )
  .add('disabled', () =>
    <div>
      <Switch
        {...actionProps}
        checked={boolean('Checked', false)}
        disabled={boolean('Disabled', true)}
      />
    </div>
  );
