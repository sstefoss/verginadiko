import Input from './index';
import InputAction from './action/index';

describe('<Input />', () => {
  it('should match snapshot', () => {
    const comp = renderer.create(
      <Input
        inactive
        validated={false}
        className='test-class'
        defaultValue='default value'
        icon='beer'
        selectedColor='f00'
        actions={[
          {
            key: 1,
            icon: 'beer'
          }
        ]}
      />
    );
    expect(comp).toMatchSnapshot();
  });

  /**
   * Test props
   */

  it('should set correct classNames', () => {
    const wrapper = shallow(<Input className='test-class' />);
    expect(wrapper.hasClass('c-input')).toEqual(true);
    expect(wrapper.hasClass('c-input--md')).toEqual(true);
    expect(wrapper.hasClass('c-input--empty')).toEqual(true);
    const inp = wrapper.find('input');
    expect(inp.hasClass('test-class')).toEqual(true);
  });

  it('should set value in input', () => {
    const wrapper = shallow(<Input value='test value' />);
    expect(wrapper.find('input').props().value).toEqual('test value');
    expect(wrapper.find('input').props().defaultValue).toEqual(undefined);
  });

  it('should set default value in input', () => {
    const wrapper = shallow(<Input defaultValue='Default value' />);
    expect(wrapper.find('input').props().defaultValue).toEqual('Default value');
    expect(wrapper.find('input').props().value).toEqual(undefined);
  });

  it('should render an invalid input', () => {
    const wrapper = shallow(<Input validated={false} />);
    expect(wrapper.hasClass('c-input--invalid')).toEqual(true);
  });

  it('should set input as inactive', () => {
    const onFocus = jest.fn();
    const wrapper = shallow(<Input inactive onFocus={onFocus} />);
    expect(wrapper.hasClass('c-input--inactive')).toEqual(true);
    expect(wrapper.find('input').props().disabled).toEqual(true);
    wrapper.find('input').simulate('focus', {
      preventDefault: () => {},
      stopPropagation: () => {}
    });
    expect(onFocus).not.toHaveBeenCalled();
  });

  it('should render empty input', () => {
    const wrapper = shallow(<Input />);
    expect(wrapper.state().empty).toEqual(true);
  });

  it('should focus on mount', () => {
    const onFocus = jest.fn();
    const wrapper = mount(<Input focusOnMount onFocus={onFocus} />);
    expect(wrapper.find('input').instance()).toEqual(document.activeElement);
  });

  it('should render input actions', () => {
    const actions = [{ key: 1 }, { key: 2 }];
    const wrapper = shallow(
      <Input actions={actions} validated={false} inactive />
    );
    expect(wrapper.hasClass('c-input--w-actions')).toEqual(true);
    expect(
      wrapper.find('input').hasClass('c-input__input--has-actions')
    ).toEqual(true);
    expect(wrapper.find('.c-input__actions-wrapper').length).toEqual(1);
    expect(wrapper.find('ul').children().length).toEqual(2);
    expect(wrapper.find(InputAction).length).toEqual(2);
    const inputAction = wrapper.find(InputAction).at(0);
    expect(inputAction.props().invalid).toEqual(true);
    expect(inputAction.props().size).toEqual('md');
    expect(inputAction.props().inputFocused).toEqual(false);
    expect(inputAction.props().disabled).toEqual(true);
  });

  it('should add "Enter to add" hint when passed as props', () => {
    const wrapper = shallow(
      <Input
        enterToAddHint={
          <span className='input-hint'>
            <strong>Enter</strong> to add
          </span>
        }
      />
    );
    expect(wrapper.find('p.c-input__hint').length).toEqual(1);
    expect(
      wrapper.contains(
        <p className='c-input__hint'>
          <span className='input-hint'>
            <strong>Enter</strong> to add
          </span>
        </p>
      )
    ).toEqual(true);
  });

  it('should render color indicator', () => {
    const wrapper = shallow(<Input selectedColor='f00' />);
    expect(wrapper.hasClass('c-input--w-color')).toEqual(true);
    expect(wrapper.find('.c-input__color').props().style.background).toEqual(
      '#f00'
    );
  });

  it('should render placeholder', () => {
    const wrapper = shallow(<Input placeholder='Placeholder' />);
    expect(wrapper.find('input').props().placeholder).toEqual('Placeholder');
  });

  it('should render read only Input component', () => {
    const wrapper = shallow(<Input textOnly value='Read only' />);
    expect(wrapper.hasClass('c-input--text-only'));
    const label = wrapper.find('.c-input__text-label');
    expect(label.text()).toEqual('Read only');
    const input = wrapper.find('input');
    expect(input.length).toEqual(0);
  });

  /**
   * Test events
   */

  it('should trigger onFocus event', () => {
    const onFocus = jest.fn();
    const wrapper = mount(<Input onFocus={onFocus} />);
    wrapper.find('input').simulate('focus');
    expect(onFocus).toHaveBeenCalled();
    expect(wrapper.state().focused).toEqual(true);
  });

  it('should trigger onBlur event', () => {
    const onBlur = jest.fn();
    const wrapper = mount(<Input onBlur={onBlur} />);
    wrapper.find('input').simulate('focus');
    wrapper.find('input').simulate('blur');
    expect(wrapper.state().focused).toEqual(false);
    expect(onBlur).toHaveBeenCalled();
  });

  it('should trigger onClick event', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<Input onClick={onClick} />);
    wrapper.find('input').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });

  it('should trigger onKeyDown event', () => {
    const onKeyDown = jest.fn();
    const wrapper = shallow(<Input onKeyDown={onKeyDown} />);
    wrapper.find('input').simulate('keyDown');
    expect(onKeyDown).toHaveBeenCalled();
  });

  it('should trigger onKeyUp event', () => {
    const onKeyUp = jest.fn();
    const wrapper = shallow(<Input onKeyUp={onKeyUp} />);
    wrapper.find('input').simulate('keyUp');
    expect(onKeyUp).toHaveBeenCalled();
  });

  it('should trigger onChange event', () => {
    const onChange = jest.fn();
    const wrapper = mount(<Input onChange={onChange} defaultValue='' />);
    wrapper.find('input').simulate('change');
    expect(onChange).toHaveBeenCalled();
    setTimeout(() => expect(wrapper.state().empty).toEqual(true), 10);
  });
});
