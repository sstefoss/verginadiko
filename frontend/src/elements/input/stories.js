import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, text, select } from '@storybook/addon-knobs/react';

import Input from './index';

const sizes = ['md', 'lg'];

storiesOf('Input', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', '') }]}
        defaultValue={text('Default value', 'Default input value')}
        enterToAddHint={boolean('Enter hint', false)}
        size={select('Size', sizes, 'md')}
        value={text('Value', undefined)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        placeholder={text('Placeholder', '')}
        selectedColor={text('Color', undefined)}
        textOnly={boolean('Text only', false)}
      />
    </div>
  ))
  .add('with actions', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', 'gears') }]}
        defaultValue={text('Default value', 'Default input value')}
        enterToAddHint={boolean('Enter hint', false)}
        size={select('Size', sizes, 'md')}
        value={text('Value', undefined)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        placeholder={text('Placeholder', '')}
        selectedColor={text('Color', undefined)}
        textOnly={boolean('Text only', false)}
      />
    </div>
  ))
  .add('with default value', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', '') }]}
        defaultValue={text('Default value', 'Default input value')}
        enterToAddHint={boolean('Enter hint', false)}
        size={select('Size', sizes, 'md')}
        value={text('Value', undefined)}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        placeholder={text('Placeholder', '')}
        selectedColor={text('Color', undefined)}
        textOnly={boolean('Text only', false)}
      />
    </div>
  ))
  .add('with "enter to add" hint (on focus)', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', '') }]}
        defaultValue={text('Default value', 'Default input value')}
        enterToAddHint={boolean('Enter hint', true)}
        size={select('Size', sizes, 'md')}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        value={text('Value', undefined)}
        placeholder={text('Placeholder', '')}
        selectedColor={text('Color', undefined)}
        textOnly={boolean('Text only', false)}
      />
    </div>
  ))
  .add('inactive', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', '') }]}
        defaultValue={text('Default value', 'Default input value')}
        enterToAddHint={boolean('Enter hint', false)}
        size={select('Size', sizes, 'md')}
        inactive={boolean('Inactive', true)}
        validated={boolean('Validated', true)}
        value={text('Value', undefined)}
        placeholder={text('Placeholder', '')}
        selectedColor={text('Color', undefined)}
        textOnly={boolean('Text only', false)}
      />
    </div>
  ))
  .add('invalid', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', '') }]}
        defaultValue={text('Default value', 'Default input value')}
        enterToAddHint={boolean('Enter hint', false)}
        size={select('Size', sizes, 'md')}
        inactive={boolean('Inactive', false)}
        value={text('Value', undefined)}
        validated={boolean('Validated', false)}
        placeholder={text('Placeholder', '')}
        selectedColor={text('Color', undefined)}
        textOnly={boolean('Text only', false)}
      />
    </div>
  ))
  .add('with placeholder', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', '') }]}
        defaultValue=''
        enterToAddHint={boolean('Enter hint', false)}
        size={select('Size', sizes, 'md')}
        inactive={boolean('Inactive', false)}
        value={text('Value', undefined)}
        validated={boolean('Validated', true)}
        placeholder={text('Placeholder', 'Input placeholder')}
        selectedColor={text('Color', undefined)}
        textOnly={boolean('Text only', false)}
      />
    </div>
  ))
  .add('with color', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', '') }]}
        defaultValue=''
        enterToAddHint={boolean('Enter hint', false)}
        size={select('Size', sizes, 'md')}
        inactive={boolean('Inactive', false)}
        value={text('Value', undefined)}
        validated={boolean('Validated', true)}
        selectedColor={text('Color', 'f00')}
        placeholder={text('Placeholder', '')}
        textOnly={boolean('Text only', false)}
      />
    </div>
  ))
  .add('with size', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', '') }]}
        defaultValue={text('Default value', 'Default input value')}
        enterToAddHint={boolean('Enter hint', false)}
        size={select('Size', sizes, 'md')}
        inactive={boolean('Inactive', false)}
        value={text('Value', undefined)}
        validated={boolean('Validated', true)}
        placeholder={text('Placeholder', '')}
        selectedColor={text('Color', undefined)}
        textOnly={boolean('Text only', false)}
      />
    </div>
  ))
  .add('with value', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', '') }]}
        defaultValue={text('Default value', 'Default input value')}
        enterToAddHint={boolean('Enter hint', false)}
        size={select('Size', sizes, 'md')}
        value={text('Value', 'Input value')}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        placeholder={text('Placeholder', '')}
        selectedColor={text('Color', undefined)}
        textOnly={boolean('Text only', false)}
      />
    </div>
  ))
  .add('text only', () => (
    <div>
      <Input
        actions={[{ key: 1, icon: text('Action icon', '') }]}
        defaultValue={text('Default value', 'Default input value')}
        enterToAddHint={boolean('Enter hint', false)}
        size={select('Size', sizes, 'md')}
        value={text('Value', 'Input value')}
        inactive={boolean('Inactive', false)}
        validated={boolean('Validated', true)}
        placeholder={text('Placeholder', '')}
        selectedColor={text('Color', undefined)}
        textOnly={boolean('Text only', true)}
      />
    </div>
  ));
