import InputAction from './action/index';
const COMPONENT = 'c-input';

import './style';

export default class Input extends React.PureComponent {
  static propTypes = {
    actions: PropTypes.array,
    className: PropTypes.string,
    defaultValue: PropTypes.string,
    enterToAddHint: PropTypes.object,
    inactive: PropTypes.bool,
    focusOnMount: PropTypes.bool,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onClick: PropTypes.func,
    onChange: PropTypes.func,
    onKeyUp: PropTypes.func,
    onKeyDown: PropTypes.func,
    placeholder: PropTypes.string,
    selectedColor: PropTypes.string,
    size: PropTypes.oneOf(['md', 'lg']),
    validated: PropTypes.bool,
    value: PropTypes.string,
    textOnly: PropTypes.bool
  };

  static defaultProps = {
    actions: [],
    className: undefined,
    defaultValue: undefined,
    enterToAddHint: null,
    validated: true,
    placeholder: '',
    selectedColor: null,
    size: 'md',
    value: undefined,
    textOnly: false,
    onKeyDown: () => {},
    onKeyUp: () => {},
    onBlur: () => {},
    onFocus: () => {},
    onChange: () => {},
    onClick: () => {}
  };

  constructor(props) {
    super(props);
    this.state = {
      focused: false,
      empty: this.isEmpty(props)
    };
  }

  get value() {
    return this.input.value;
  }

  isEmpty = props => {
    const { value, defaultValue } = props;
    if (typeof value !== 'undefined') {
      return value.length === 0;
    } else if (typeof defaultValue !== 'undefined') {
      return defaultValue.length === 0;
    }
    return true;
  };

  componentDidMount() {
    const { focusOnMount } = this.props;
    if (focusOnMount && this.input) {
      this.input.focus();
    }
  }

  componentWillReceiveProps(nextProps) {
    const empty = this.isEmpty(nextProps);
    this.setState({ empty });
  }

  onFocus = e => {
    const { inactive } = this.props;
    if (inactive) {
      e.preventDefault();
      e.stopPropagation();
      return;
    }
    this.setState({ focused: true });
    const temp = e.target.value;
    e.target.value = '';
    e.target.value = temp;
    this.props.onFocus(e);
  };

  onBlur = e => {
    this.setState({ focused: false });
    this.props.onBlur(e);
  };

  onChange = e => {
    this.setState({
      empty: this.input.value.length === 0
    });
    this.props.onChange(e);
  };

  render() {
    const {
      size,
      validated,
      inactive,
      className,
      enterToAddHint,
      actions,
      textOnly,
      defaultValue,
      selectedColor
    } = this.props;

    const { focused, empty } = this.state;

    const inputActions = actions.length > 0 && (
      <div className='c-input__actions-wrapper'>
        <ul>
          {actions.map(action => (
            <InputAction
              key={action.key ? action.key : Math.random()}
              invalid={typeof validated !== 'undefined' && !validated}
              inputFocused={focused}
              action={action}
              disabled={inactive}
              size={size}
            />
          ))}
        </ul>
      </div>
    );

    const inputCN = classNames(`${COMPONENT}__input`, className, {
      [`${COMPONENT}__input--has-actions`]: actions.length > 0,
      [`${COMPONENT}__input--is-focused`]: focused
    });

    const colorComp = selectedColor && (
      <div className={`${COMPONENT}__color-wrapper`}>
        <span
          className={`${COMPONENT}__color`}
          style={{ background: `#${selectedColor}` }}
        />
      </div>
    );

    const hintCN = classNames(`${COMPONENT}__hint`, {
      [`${COMPONENT}__hint--is-visible`]: focused && !empty
    });
    const hint = enterToAddHint && <p className={hintCN}>{enterToAddHint}</p>;

    const inputProps = {
      onChange: this.onChange,
      onBlur: this.onBlur,
      onFocus: this.onFocus,
      onKeyUp: this.props.onKeyUp,
      onKeyDown: this.props.onKeyDown,
      onClick: this.props.onClick,
      type: 'text',
      ref: ref => (this.input = ref),
      placeholder: this.props.placeholder,
      disabled: this.props.inactive,
      defaultValue: this.props.defaultValue,
      className: inputCN,
      value: this.props.value
    };
    const input = <input {...inputProps} />;

    const value = this.props.value ? this.props.value : '';
    const textOnlyComp = textOnly && (
      <div className={`${COMPONENT}__text-label`}>
        {defaultValue ? defaultValue : value}
      </div>
    );

    const inputGroupClassName = classNames(COMPONENT, `${COMPONENT}--${size}`, {
      [`${COMPONENT}--inactive`]: inactive,
      [`${COMPONENT}--empty`]: empty,
      [`${COMPONENT}--focused`]: focused,
      [`${COMPONENT}--invalid`]: typeof validated !== 'undefined' && !validated,
      [`${COMPONENT}--w-actions`]: actions.length > 0,
      [`${COMPONENT}--w-color`]: selectedColor,
      [`${COMPONENT}--text-only`]: textOnly
    });
    return (
      <div className={inputGroupClassName}>
        {colorComp}
        {textOnly && textOnlyComp}
        {!textOnly && input}
        {inputActions}
        {hint}
      </div>
    );
  }
}
