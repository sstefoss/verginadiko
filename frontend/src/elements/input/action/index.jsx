import Icon from '../../icon/index';
const COMPONENT = 'c-input-action';
import './style';

export default class InputAction extends React.PureComponent {
  static propTypes = {
    action: PropTypes.shape({
      key: PropTypes.any,
      icon: PropTypes.string,
      onClick: PropTypes.func,
      element: PropTypes.node
    }),
    size: PropTypes.string,
    invalid: PropTypes.bool,
    inputFocused: PropTypes.bool
  };

  static defaultProps = {
    action: {
      key: Math.random(),
      icon: '',
      onClick: () => {},
      onMouseDown: () => {},
      element: null
    }
  };

  render() {
    const { size, action, invalid, inputFocused } = this.props;

    const inputAddOnCN = classNames(COMPONENT, {
      [`${COMPONENT}--${size}`]: true,
      [`${COMPONENT}--invalid`]: invalid,
      [`${COMPONENT}--input-focused`]: inputFocused
    });

    return (
      <li key={action.key} className={inputAddOnCN}>
        <div
          onClick={action.onClick}
          onMouseDown={action.onMouseDown}
          className={`${COMPONENT}__wrapper`}
        >
          <div className={`${COMPONENT}__element`}>{action.element}</div>
          <Icon className={`${COMPONENT}__icon`} icon={action.icon} />
        </div>
      </li>
    );
  }
}
