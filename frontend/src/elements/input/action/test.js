import InputAction from './index';

describe('<InputAction />', () => {
  it('should render', () => {
    const comp = renderer.create(
      <InputAction
        size='md'
        invalid
        inputFocused
        action={{
          key: 'test-action',
          icon: 'beer',
          element: (
            <div>action element</div>
          )
        }}
      />
    );
    expect(comp).toMatchSnapshot();
  });

  it('should trigger onClick event on action', () => {
    const onClick = jest.fn();
    const wrapper = shallow(
      <InputAction
        action={{
          key: 'test-action',
          icon: 'beer',
          onClick: onClick,
        }}
      />
    );
    wrapper.find('.c-input-action__wrapper').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });

  it('should render action element', () => {
    const wrapper = shallow(
      <InputAction
        action={{
          key: 'test-action',
          icon: 'beer',
          element: (
            <div className='test-el'>test</div>
          )
        }}
      />
    );
    expect(wrapper.find('.test-el').length).toEqual(1);
  });
});
