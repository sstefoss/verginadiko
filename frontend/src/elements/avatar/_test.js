import Avatar from './index';

const user = {
  id: 'UASDA12',
  email: 'test@test.io',
  avatar: 'avatar_url',
  realname: 'Test test',
  username: 'username'
};

describe('<Avatar />', () => {
  it('should render', () => {
    const comp = renderer.create(
      <Avatar user={user} />
    ).toJSON();
    expect(comp).toMatchSnapshot();
  });

  it('should add className passed from props', () => {
    const wrapper = shallow(
      <Avatar
        user={user}
        className='test-class'
      />
    );
    expect(wrapper.hasClass('test-class')).toEqual(true);
  });
});
