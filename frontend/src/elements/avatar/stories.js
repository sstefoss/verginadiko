import {storiesOf} from '@storybook/react';
import {withKnobs, select, text} from '@storybook/addon-knobs/react';

import Avatar from './index';

const sizes = ['xlg', 'lg', 'md', 'sm', 'xs'];

const avatar = 'https://secure.gravatar.com/avatar/1f9b2f5b51370ebfdbc7bbae12622111.jpg?s=192&d=https%3A%2F%2Fa.slack-edge.com%2F7fa9%2Fimg%2Favatars%2Fava_0024-192.png';

storiesOf('Avatar', module)
  .addDecorator(withKnobs)
  .add('default', () =>
    <div>
      <Avatar
        size={select('Sizes', sizes, 'lg')}
        user={{
          id: text('Id', 'U00000000'),
          email: text('Email', 'user@geekbot.io'),
          username: text('Username', 'user'),
          realname: text('Realname', 'Test User'),
          avatar,
        }}
      />
    </div>
  )
  .add('with sizes', () =>
    <div>
      <Avatar
        size={select('Sizes', sizes, 'md')}
        user={{
          id: text('Id', 'U00000000'),
          email: text('Email', 'user@geekbot.io'),
          username: text('Username', 'user'),
          realname: text('Realname', 'Test User'),
          avatar,
        }}
      />
    </div>
  );
