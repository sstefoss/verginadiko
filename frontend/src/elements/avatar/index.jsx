const COMPONENT = 'c-avatar';
import './style';

export default class Avatar extends React.PureComponent {

  static propTypes = {
    className: PropTypes.string,
    size: PropTypes.oneOf(['xlg', 'lg', 'md', 'sm', 'xs']),
    user: PropTypes.shape({
      id: PropTypes.string,
      username: PropTypes.string,
      realname: PropTypes.string,
      email: PropTypes.string,
      avatar: PropTypes.string,
    }).isRequired,
  }

  static defaultProps = {
    className: null,
    size: 'lg',
  }

  render() {
    const {user, className, size} = this.props;
    const cn = classNames(COMPONENT, `${COMPONENT}--${size}`, className);
    return (
      <img
        alt={user.username}
        className={cn}
        src={user.avatar}
        title={`@${user.username}`}
       />
    );
  }
}
