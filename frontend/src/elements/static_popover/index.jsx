const COMPONENT = 'c-static-popover';
import './style';

export default class StaticPopover extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    dest: PropTypes.oneOf(['bottom', 'top', 'right', 'left', 'none']),
    className: PropTypes.string,
  };

  static defaultProps = {
    dest: 'bottom',
    className: null,
    children: null,
  };

  render() {
    const { dest, children, className } = this.props;
    const cn = classNames(COMPONENT, className, {
      [`${COMPONENT}--${dest}`]: dest
    });
    return (
      <div className={cn}>
        <div className={`${COMPONENT}__arrow`} />
        <div className={`${COMPONENT}__content`}>{children}</div>
      </div>
    );
  }
}
