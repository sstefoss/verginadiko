import {storiesOf} from '@storybook/react';
import {withKnobs} from '@storybook/addon-knobs/react';

import StaticPopover from './index';

storiesOf('StaticPopover', module)
  .addDecorator(withKnobs)
  .add('default', () =>
    <div style={{marginTop: '50px'}}>
      <StaticPopover>
        Static popover content
      </StaticPopover>
    </div>
  );
