import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Signup from './views/signup/index';
import { MAPFIT_API_KEY } from './constants';
import './App.css';

export default class App extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]).isRequired
  };

  componentDidMount() {
    window.mapfit.apikey = MAPFIT_API_KEY;
    window.geo = new window.mapfit.Geocoder(
      MAPFIT_API_KEY,
      'https://api.mapfit.com/v2'
    );
  }

  render() {
    const { children } = this.props;
    return (
      <div className='App'>
        <header className='header'>
          <h1 className='title'>
            <Link to='/'>Verginadiko</Link>
          </h1>
          <Signup />
        </header>
        <main>{children}</main>
      </div>
    );
  }
}
