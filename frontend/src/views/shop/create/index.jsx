export default class UC extends React.Component {
  render() {
    return null;
  }
}

// import React from "react";
// import { graphql, compose } from "react-apollo";
// import { withRouter } from "react-router-dom";

// import { createShop } from "./graphql";

// import { withStyles } from "@material-ui/core/styles";
// import TextField from "@material-ui/core/TextField";
// import Button from "@material-ui/core/Button";
// import "./style.css";

// const styles = theme => ({
//   textField: {
//     marginLeft: theme.spacing.unit,
//     marginRight: theme.spacing.unit
//   }
// });

// class ShopCreate extends React.Component {
//   state = {
//     address: "",
//     city: "",
//     location: null,
//     marker: null,
//   };

//   componentDidMount() {
//     const map = window.mapfit.MapView("mapfit", {
//       theme: "night",
//       maxZoon: 14
//     });
//     map.setZoom(13);
//     // set map center with coordinate
//     map.setCenter([40.637, 22.951]);
//     map.on("click", e => {
//       const lat = e.latlng.lat;
//       const lng = e.latlng.lng;
//       window.geo
//         .reverseGeocode([lat, lng])
//         .then(data => {
//           // remove previous marker
//           if (this.state.marker) {
//             map.removeMarker(this.state.marker);
//           }

//           // set new state
//           const marker = window.mapfit.Marker([lat, lng]);
//           const address = data[0].street_address;
//           const city = data[0].locality;
//           const location = data[0].location;

//           map.addMarker(marker);
//           this.setState({ address, city, location, marker });
//         })
//         .catch(error => console.log(" error = ", error));
//     });
//   }

//   onSubmit = async e => {
//     e.preventDefault();
//     const { history } = this.props;
//     const { address, city, location } = this.state;
//     const name = this.nameInp.value;
//     const opens = this.opensInp.value;
//     const closes = this.closesInp.value;
//     const bottlePrice = parseFloat(this.bottlePriceInp.value);
//     const result = await this.props.createShop({
//       variables: {
//         name,
//         opens,
//         closes,
//         address,
//         city,
//         location,
//         bottlePrice
//       }
//     });
//     const id = result.data.createShop.id;
//     history.push(`/shop/view/${id}`);
//   };

//   render() {
//     const { address, city } = this.state;
//     return (
//       <section className="create">
//         <div id="mapfit" className="map" />
//         <div className="shop-form">
//           <h2>Create Shop</h2>
//           <form onSubmit={this.onSubmit}>
//             <TextField
//               id="opens"
//               className={this.props.classes.textField}
//               label="Ανοίγει"
//               type="time"
//               defaultValue="07:30"
//               margin="normal"
//               fullWidth
//               inputRef={ref => (this.opensInp = ref)}
//             />
//             <TextField
//               id="closes"
//               className={this.props.classes.textField}
//               label="Κλείνει"
//               defaultValue="22:00"
//               type="time"
//               margin="normal"
//               fullWidth
//               inputRef={ref => (this.closesInp = ref)}
//             />
//             <TextField
//               id="name"
//               inputRef={ref => (this.nameInp = ref)}
//               className={this.props.classes.textField}
//               label="Όνομα"
//               margin="normal"
//               fullWidth
//             />
//             <TextField
//               id="address"
//               className={this.props.classes.textField}
//               label="Διεύθυνση"
//               value={address}
//               margin="normal"
//               fullWidth
//             />
//             <TextField
//               id="city"
//               className={this.props.classes.textField}
//               label="Πόλη"
//               value={city}
//               margin="normal"
//               fullWidth
//             />
//             <TextField
//               id="bottle-price"
//               inputRef={ref => (this.bottlePriceInp = ref)}
//               className={this.props.classes.textField}
//               label="Τιμή μπουκαλιού"
//               margin="normal"
//               fullWidth
//             />
//             <Button variant="outlined" type="submit">
//               Έγινε
//             </Button>
//           </form>
//         </div>
//       </section>
//     );
//   }
// }

// export default compose(
//   graphql(createShop, { name: "createShop" }),
//   withStyles(styles),
//   withRouter
// )(ShopCreate);
