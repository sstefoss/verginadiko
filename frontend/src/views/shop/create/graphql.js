import gql from "graphql-tag";

export const createShop = gql`
  mutation createShop($name: String, $address: String, $city: String, $location: LocationInput!, $opens: String!, $closes: String!, $bottlePrice: Float) {
    createShop(name: $name, city: $city, address: $address, opens: $opens, closes: $closes, location: $location, bottlePrice: $bottlePrice) {
      id
    }
  }
`;
