export default class UC extends React.Component {
  render() {
    return null;
  }
}

// import React from "react";
// import { graphql, compose } from "react-apollo";
// import { fetchShop } from "./graphql";
// import { withRouter } from "react-router-dom";
// import MapView from './map';
// import Info from './info';

// // import { withStyles } from "@material-ui/core/styles";
// import Grid from "@material-ui/core/Grid";

// class ShopView extends React.Component {
//   state = {
//     marker: null,
//     location: null,
//     address: "",
//     city: "",
//   }

//   onClick = (map, marker) => {
//     this.setState({ marker });
//     map.addMarker(marker);

//     map.on("click", e => {
//       const lat = e.latlng.lat;
//       const lng = e.latlng.lng;
//       window.geo
//         .reverseGeocode([lat, lng])
//         .then(data => {
//           // remove previous marker
//           if (this.state.marker) {
//             map.removeMarker(this.state.marker);
//           }

//           // set new state
//           const marker = window.mapfit.Marker([lat, lng]);
//           const address = data[0].street_address;
//           const city = data[0].locality;
//           const location = data[0].location;

//           map.addMarker(marker);
//           this.setState({ address, city, location, marker });
//         })
//         .catch(error => console.log(" error = ", error));
//     });
//   }

//   render() {
//     const { data } = this.props;
//     if (data.loading) {
//       return null;
//     }

//     return (
//       <Grid container>
//         <Grid item xs={8}>
//           <MapView
//             location={data.shop.location}
//             onClick={this.onClick}
//           />
//         </Grid>
//         <Grid item xs={4}>
//           <Info
//             shop={data.shop}
//             newCity={this.state.city}
//             newAddress={this.state.address}
//             newLocation={this.state.location}
//           />
//         </Grid>
//       </Grid>
//     );
//   }
// }

// export default compose(
//   graphql(fetchShop, {
//     options: props => ({
//       variables: {
//         id: props.match.params.id
//       }
//     })
//   }),
//   // withStyles(styles),
//   withRouter
// )(ShopView);
