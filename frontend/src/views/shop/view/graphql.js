import gql from "graphql-tag";

export const fetchShop = gql`
  query fetchShop($id: ID!) {
    shop(id: $id) {
      id
      name
      opens
      closes
      location {
        lat
        lon
      }
      address
      name,
      bottlePrice,
      city,
      beers {
        id
        type
        size
        container
        price
      }
    }
  }
`;

export const updateShop = gql`
  mutation updateShop($id: ID!, $name: String, $location: LocationInput!, $opens: String!, $closes: String!, $bottlePrice: Float) {
    updateShop(id: $id, name: $name, opens: $opens, closes: $closes, location: $location, bottlePrice: $bottlePrice) {
      id
    }
  }
`;

export const deleteShop = gql`
  mutation deleteShop($id: ID!) {
    deleteShop(id: $id) {
      id
    }
  }
`;
