export default class UC extends React.Component {
  render() {
    return null;
  }
}

// import React from "react";
// import { compose, graphql } from "react-apollo";
// import { withRouter } from "react-router-dom";
// import Grid from "@material-ui/core/Grid";
// import { withStyles } from "@material-ui/core";
// import Button from "@material-ui/core/Button";
// import TextField from "@material-ui/core/TextField";

// import BeerCreate from "../../beer/create";
// import BeerList from "../../beer/list";
// import { deleteShop, updateShop, fetchShop } from "./graphql";
// import { fetchShops } from "../../map/graphql";

// const styles = theme => ({
//   textField: {
//     marginLeft: theme.spacing.unit,
//     marginRight: theme.spacing.unit,
//   }
// });

// class ShopInfo extends React.Component {
//   state = {
//     newBeer: false,
//   };

//   onCreateBeer = () => this.setState({ newBeer: true });
//   onBeerCreated = () => this.setState({ newBeer: false });
//   onDeleteClicked = () => {
//     const { history } = this.props;
//     const { id } = this.props.shop;
//     this.props.deleteShop({
//       variables: {
//         id
//       },
//       optimisticResponse: {
//         deleteShop: {
//           __typename: "Shop",
//           id
//         }
//       },
//       update: (store, { data: { deleteShop } }) => {
//         try {
//           const stored = store.readQuery({ query: fetchShops });
//           const newCollection = stored.shops.filter(
//             shop => shop.id !== deleteShop.id
//           );
//           stored.shops = newCollection;
//           store.writeQuery({ query: fetchShops, data: stored });
//         } catch (e) {
//           return null;
//         }
//       }
//     });
//     history.push("/");
//   };

//   onSubmit = async e => {
//     e.preventDefault();
//     const { id } = this.props.shop;
//     const name = this.nameInp.value;
//     const opens = this.opensInp.value;
//     const closes = this.closesInp.value;
//     const city = this.props.newCity;
//     const address = this.props.newAddress;
//     const location = this.props.newLocation;
//     console.log(address);
//     const bottlePrice = parseFloat(this.bottlePriceInp.value);
//     this.props.updateShop({
//       variables: {
//         id,
//         name,
//         opens,
//         closes,
//         address,
//         city,
//         location,
//         bottlePrice
//       },
//       optimisticResponse: {
//         __typename: "Mutation",
//         updateShop: {
//           __typename: "Shop",
//           id,
//           name,
//           opens,
//           closes,
//           address,
//           city,
//           location,
//           bottlePrice
//         }
//       },
//       update: (store, { data: { updateShop } }) => {
//         try {
//           const stored = store.readQuery({ query: fetchShop });
//           const newCollection = [
//             ...stored.shop,
//             {
//               __typename: "Shop",
//               id: updateShop.id,
//               name: updateShop.name,
//               opens: updateShop.opens,
//               closes: updateShop.closes,
//               address: updateShop.address,
//               city: updateShop.city,
//               location: updateShop.location,
//               bottlePrice: updateShop.bottlePrice
//             }
//           ];
//           stored.shop = newCollection;
//           store.writeQuery({ query: fetchShop, data: stored });
//         } catch (e) {
//           return null;
//         }
//         return null;
//       }
//     });
//   };

//   render() {
//     const {
//       id,
//       name,
//       opens,
//       closes,
//       address,
//       city,
//       beers,
//       bottlePrice
//     } = this.props.shop;
//     const { newBeer } = this.state;
//     return (
//       <Grid container>
//         <Grid item xs={12}>
//           <form onSubmit={this.onSubmit}>
//             <TextField
//               id="name"
//               className={this.props.classes.textField}
//               label="Όνομα"
//               margin="normal"
//               fullWidth
//               defaultValue={name}
//               inputRef={ref => (this.nameInp = ref)}
//             />
//             <TextField
//               id="opens"
//               className={this.props.classes.textField}
//               type="time"
//               label="Ανοίγει"
//               margin="normal"
//               fullWidth
//               defaultValue={opens}
//               inputRef={ref => (this.opensInp = ref)}
//             />
//             <TextField
//               id="closes"
//               className={this.props.classes.textField}
//               type="time"
//               label="Κλείνει"
//               margin="normal"
//               fullWidth
//               defaultValue={closes}
//               inputRef={ref => (this.closesInp = ref)}
//             />
//             <TextField
//               id="address"
//               className={this.props.classes.textField}
//               label="Διεύθυνση"
//               margin="normal"
//               fullWidth
//               defaultValue={address}
//             />
//             <TextField
//               id="city"
//               className={this.props.classes.textField}
//               label="Πόλη"
//               margin="normal"
//               fullWidth
//               defaultValue={city}
//             />
//             <TextField
//               id="bottle-price"
//               className={this.props.classes.textField}
//               label="Τιμή μπουκαλιού"
//               margin="normal"
//               fullWidth
//               defaultValue={bottlePrice}
//               inputRef={ref => (this.bottlePriceInp = ref)}
//             />
//             <Button type="submit">
//               EDIT
//             </Button>
//           </form>
//         </Grid>
//         <Grid item xs={12}>
//           <BeerList beers={beers} />
//           <Button onClick={this.onCreateBeer}>Βαλε μπύρα</Button>
//           {newBeer && (
//             <BeerCreate shopId={id} onBeerCreated={this.onBeerCreated} />
//           )}
//           <Button onClick={this.onDeleteClicked}>Διαγραφή</Button>
//         </Grid>
//       </Grid>
//     );
//   }
// }

// export default compose(
//   withRouter,
//   withStyles(styles),
//   graphql(updateShop, { name: "updateShop"}),
//   graphql(deleteShop, { name: "deleteShop" })
// )(ShopInfo);
