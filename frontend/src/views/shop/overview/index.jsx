import BeerOverview from '../../beer/overview';
const COMPONENT = 'c-shop-overview';
import './style';

export default class ShopOverview extends React.Component {
  static propTypes = {
    shop: PropTypes.object
  };

  render() {
    const { shop } = this.props;
    console.info(this.props.shop);
    return (
      <div className={COMPONENT}>
        <div className={`${COMPONENT}__beers`}>
          {shop.beers.map(beer => (
            <BeerOverview beer={beer} />
          ))}
        </div>
        {/* <div className={`${COMPONENT}__returning-bottle`} /> */}
        <div className={`${COMPONENT}__location`}>
          <div className={`${COMPONENT}__address`} />
          <div className={`${COMPONENT}__distance`} />
        </div>
        <div className={`${COMPONENT}__hours`}>
          <div className={`${COMPONENT}__opening`} />
          <div className={`${COMPONENT}__closing`} />
        </div>
      </div>
    );
  }
}
