import gql from "graphql-tag";

export const fetchBeerTypes = gql`
  query fetchBeerTypes {
    __type(name: "BeerType") {
      enumValues {
        name
      }
    }
  }
`;

export const fetchBeerSizes = gql`
  query fetchBeerTypes {
    __type(name: "BeerSize") {
      enumValues {
        name
      }
    }
  }
`;

export const fetchBeerContainers = gql`
  query fetchBeerTypes {
    __type(name: "BeerContainer") {
      enumValues {
        name
      }
    }
  }
`;

export const createBeer = gql`
  mutation createBeer(
    $price: Float!
    $type: BeerType!
    $container: BeerContainer!
    $size: BeerSize
    $shopId: String!
  ) {
    createBeer(
      price: $price
      type: $type
      container: $container
      size: $size
      shopId: $shopId
    ) {
      id
      type
      size
      container
      price
    }
  }
`;
