export default class UC extends React.Component {
  render() {
    return null;
  }
}

// import React from 'react';
// import PropTypes from 'prop-types';
// import { compose, graphql } from 'react-apollo';

// import { withStyles } from '@material-ui/core';
// import TextField from '@material-ui/core/TextField';
// import Button from '@material-ui/core/Button';
// import MenuItem from '@material-ui/core/MenuItem';
// import {
//   createBeer,
//   fetchBeerTypes,
//   fetchBeerContainers,
//   fetchBeerSizes
// } from './graphql';

// import { fetchShop } from '../shop/view/graphql';

// const enums = {
//   TYPES: 'types',
//   CONTAINERS: 'containers',
//   SIZES: 'sizes'
// };

// const styles = theme => ({
//   textField: {
//     marginLeft: theme.spacing.unit,
//     marginRight: theme.spacing.unit
//   },
//   menu: {
//     width: 200
//   }
// });

// class CreateBeer extends React.Component {
//   static propTypes = {
//     shopId: PropTypes.string,
//     onBeerCreated: PropTypes.func
//   };

//   state = {
//     type: null,
//     size: null,
//     container: null
//   };

//   componentWillReceiveProps(nextProps) {
//     if (this.requestsLoading(this.props) && !this.requestsLoading(nextProps)) {
//       this.setState({
//         type: nextProps[enums.TYPES].__type.enumValues[0].name,
//         size: nextProps[enums.SIZES].__type.enumValues[0].name,
//         container: nextProps[enums.CONTAINERS].__type.enumValues[0].name
//       });
//     }
//   }

//   requestsLoading = props => {
//     const reqs = Object.keys(enums).map(val => props[enums[val]].loading);
//     const loading = reqs.filter(val => val);
//     return loading.length !== 0;
//   };

//   onDDChange = (key, value) => {
//     this.setState({ [key]: value });
//   };

//   onSubmit = e => {
//     e.preventDefault();
//     const { createBeer, shopId, onBeerCreated } = this.props;
//     const price = parseFloat(this.priceInp.value);
//     const { size, type, container } = this.state;
//     createBeer({
//       variables: {
//         price,
//         size,
//         type,
//         container,
//         shopId
//       },
//       optimisticResponse: {
//         __typename: 'Mutation',
//         createBeer: {
//           id: null,
//           __typename: 'Beer',
//           price,
//           size,
//           container,
//           type
//         }
//       },
//       update: (store, { data: { createBeer } }) => {
//         try {
//           const stored = store.readQuery({
//             query: fetchShop,
//             variables: {
//               id: shopId
//             }
//           });
//           const newCollection = [
//             ...stored.shop.beers,
//             {
//               __typename: 'Beer',
//               id: createBeer.id,
//               price: createBeer.price,
//               type: createBeer.type,
//               size: createBeer.size,
//               container: createBeer.container
//             }
//           ];
//           stored.shop.beers = newCollection;
//           store.writeQuery({ query: fetchShop, data: stored });
//         } catch (e) {
//           return null;
//         }
//         return null;
//       }
//     });
//     onBeerCreated();
//   };

//   render() {
//     if (this.requestsLoading(this.props)) {
//       return null;
//     }
//     const { classes } = this.props;
//     const { type, size, container } = this.state;
//     const enumTypes = this.props[enums.TYPES].__type.enumValues;
//     const enumSizes = this.props[enums.SIZES].__type.enumValues;
//     const enumContainers = this.props[enums.CONTAINERS].__type.enumValues;

//     return (
//       <section>
//         <div className='beer-form'>
//           <h2>Insert Beer</h2>
//           <form onSubmit={this.onSubmit}>
//             <TextField
//               id='price'
//               className={this.props.classes.textField}
//               label='Price'
//               margin='normal'
//               fullWidth
//               defaultValue={1.2}
//               inputRef={ref => (this.priceInp = ref)}
//             />
//             <TextField
//               select
//               id='beerType'
//               className={this.props.classes.textField}
//               label='Beer type'
//               margin='normal'
//               fullWidth
//               value={type}
//               inputRef={ref => (this.typeInp = ref)}
//               onChange={e => this.onDDChange('type', e.target.value)}
//               SelectProps={{
//                 MenuProps: {
//                   className: classes.menu
//                 }
//               }}
//             >
//               {enumTypes.map(type => (
//                 <MenuItem key={type.name} value={type.name}>
//                   {type.name}
//                 </MenuItem>
//               ))}
//             </TextField>
//             <TextField
//               select
//               id='beerContainer'
//               className={this.props.classes.textField}
//               label='Beer container'
//               margin='normal'
//               value={container}
//               fullWidth
//               onChange={e => this.onDDChange('container', e.target.value)}
//               inputRef={ref => (this.opensInp = ref)}
//             >
//               {enumContainers.map(container => (
//                 <MenuItem key={container.name} value={container.name}>
//                   {container.name}
//                 </MenuItem>
//               ))}
//             </TextField>
//             <TextField
//               select
//               id='beerSize'
//               className={this.props.classes.textField}
//               label='Beer size'
//               value={size}
//               margin='normal'
//               fullWidth
//               onChange={e => this.onDDChange('size', e.target.value)}
//               inputRef={ref => (this.opensInp = ref)}
//             >
//               {enumSizes.map(size => (
//                 <MenuItem key={size.name} value={size.name}>
//                   {size.name}
//                 </MenuItem>
//               ))}
//             </TextField>
//             <Button variant='outlined' type='submit'>
//               Έγινε
//             </Button>
//           </form>
//         </div>
//       </section>
//     );
//   }
// }

// export default compose(
//   graphql(fetchBeerTypes, { name: enums.TYPES }),
//   graphql(fetchBeerSizes, { name: enums.SIZES }),
//   graphql(fetchBeerContainers, { name: enums.CONTAINERS }),
//   graphql(createBeer, { name: 'createBeer' }),
//   withStyles(styles)
// )(CreateBeer);
