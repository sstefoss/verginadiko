export default class UC extends React.Component {
  render() {
    return null;
  }
}

// import React from "react";
// import PropTypes from "prop-types";
// import { compose } from "react-apollo";
// import { withStyles } from "@material-ui/core";

// const styles = theme => ({
//   wrapper: {
//     display: "flex",
//     flexDirection: "column",
//     padding: theme.spacing.unit,
//     margin: theme.spacing.unit,
//     border: '1px dashed #ccc'
//   },
//   type: {
//     fontSize: "18px",
//     fontWeight: 900
//   },
//   info: {
//     display: "flex",
//     justifyContent: "space-around"
//   }
// });

// class BeerList extends React.Component {
//   static propTypes = {
//     beers: PropTypes.array
//   };

//   render() {
//     const { beers, classes } = this.props;
//     return (
//       <div>
//         {beers.map(beer => (
//           <div className={classes.wrapper} key={beer.id}>
//             <div className={classes.type}>{beer.type}</div>
//             <div className={classes.info}>
//               <div>{beer.size}</div>
//               <div>{beer.container}</div>
//               <div>{beer.price}</div>
//             </div>
//           </div>
//         ))}
//       </div>
//     );
//   }
// }

// export default compose(withStyles(styles))(BeerList);
