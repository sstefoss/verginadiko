import { containerToString, typeToString } from 'utils';
import Icon from 'elements/icon';
const COMPONENT = 'c-beer-overview';
import './style';

export default class BeerOverview extends React.Component {
  static propTypes = {
    beer: PropTypes.object
  };

  render() {
    const { beer } = this.props;
    return (
      <div className={`${COMPONENT}`}>
        <div className={`${COMPONENT}__type`}>
          <Icon icon='beer' />
          <span>{typeToString(beer.type)}</span>
        </div>
        <div className={`${COMPONENT}__price`}>
          <div className={`${COMPONENT}__container`}>
            {containerToString(beer.container)}
          </div>
          <div className={`${COMPONENT}__price`}>{`$${beer.price}`}</div>
        </div>
      </div>
    );
  }
}
