export default class UC extends React.Component {
  render() {
    return null;
  }
}

// import { graphql, compose } from "react-apollo";
// import React from "react";
// import PropTypes from "prop-types";
// import { withRouter } from "react-router-dom";

// import { withStyles } from "@material-ui/core/styles";
// import Button from "@material-ui/core/Button";
// import Modal from "@material-ui/core/Modal";
// import TextField from "@material-ui/core/TextField";
// import Tabs from "@material-ui/core/Tabs";
// import Tab from "@material-ui/core/Tab";

// import { signup, login, me } from "./graphql";
// import { AUTH_TOKEN } from "../../constants";
// import { saveUserToken, isUserLoggedIn } from "../../utils";

// const LOGIN_TAB = 0;
// const SIGNUP_TAB = 1;

// const getModalStyle = () => {
//   const top = 50;
//   const left = 50;

//   return {
//     top: `${top}%`,
//     left: `${left}%`,
//     transform: `translate(-${top}%, -${left}%)`
//   };
// };

// const styles = theme => ({
//   modal: {
//     position: "absolute",
//     width: theme.spacing.unit * 50,
//     backgroundColor: theme.palette.background.paper,
//     boxShadow: theme.shadows[5],
//     padding: theme.spacing.unit * 4
//   },
//   textField: {
//     marginLeft: theme.spacing.unit,
//     marginRight: theme.spacing.unit
//   }
// });

// const TabContainer = props => {
//   return (
//     <form
//       onSubmit={e => {
//         e.preventDefault();
//         props.confirm();
//         props.handleClose();
//         props.history.push("/shop/create");
//       }}
//     >
//       {props.value === 1 && (
//         <TextField
//           id="username"
//           className={props.classes.textField}
//           label="Username"
//           value={props.username}
//           onChange={props.handleChange("username")}
//           margin="normal"
//           fullWidth
//         />
//       )}
//       <TextField
//         id="email"
//         className={props.classes.textField}
//         type="email"
//         label="Email"
//         value={props.email}
//         onChange={props.handleChange("email")}
//         margin="normal"
//         fullWidth
//       />
//       <TextField
//         id="password"
//         className={props.classes.textField}
//         type="password"
//         label="Password"
//         value={props.password}
//         onChange={props.handleChange("password")}
//         margin="normal"
//         fullWidth
//       />
//       <Button variant="outlined" type="submit">
//         {props.value === 0 ? "Connect" : "Sign up"}
//       </Button>
//     </form>
//   );
// };

// class SignupForm extends React.Component {
//   static propTypes = {
//     classes: PropTypes.object.isRequired,
//     history: PropTypes.object.isRequired
//   };

//   state = {
//     tabValue: LOGIN_TAB,
//     value: "",
//     login: false,
//     open: false,
//     username: "",
//     email: "",
//     password: ""
//   };

//   handleOpen = () => this.setState({ open: true });

//   handleClose = () => this.setState({ open: false });

//   hanldeTabChange = (e, tabValue) => this.setState({ tabValue });

//   handleChange = name => event => {
//     this.setState({
//       [name]: event.target.value
//     });
//   };

//   confirm = async () => {
//     const { username, email, password } = this.state;

//     if (this.state.tabValue === LOGIN_TAB) {
//       const result = await this.props.login({
//         variables: {
//           email,
//           password
//         }
//       });
//       const { token } = result.data.login;
//       this.setState({ login: true });
//       saveUserToken(token);
//     } else {
//       const result = await this.props.signup({
//         variables: {
//           username,
//           email,
//           password
//         }
//       });
//       const { token } = result.data.signup;
//       this.setState({ login: true });
//       saveUserToken(token);
//     }
//   };

//   isLoggedIn = () => {
//     const { loggedIn } = this.state;
//     return loggedIn || isUserLoggedIn();
//   };

//   render() {
//     const { classes } = this.props;
//     const { tabValue } = this.state;

//     return (
//       <section>
//         {this.isLoggedIn() ? (
//           <Button
//             variant="outlined"
//             onClick={e => {
//               e.preventDefault();
//               localStorage.removeItem(AUTH_TOKEN);
//               this.props.history.push("/");
//               this.setState({ login: false });
//             }}
//           >
//             Logout
//           </Button>
//         ) : (
//           <Button variant="outlined" onClick={this.handleOpen}>
//             Connect
//           </Button>
//         )}
//         <Modal open={this.state.open} onClose={this.handleClose}>
//           <div style={getModalStyle()} className={classes.modal}>
//             <Tabs value={tabValue} onChange={this.hanldeTabChange}>
//               <Tab label="Log in" />
//               <Tab label="Sign up" />
//             </Tabs>
//             {tabValue === LOGIN_TAB && (
//               <TabContainer
//                 {...this.props}
//                 value={tabValue}
//                 handleChange={this.handleChange}
//                 confirm={this.confirm}
//                 handleClose={this.handleClose}
//               />
//             )}
//             {tabValue === SIGNUP_TAB && (
//               <TabContainer
//                 {...this.props}
//                 value={tabValue}
//                 handleChange={this.handleChange}
//                 confirm={this.confirm}
//                 handleClose={this.handleClose}
//               />
//             )}
//           </div>
//         </Modal>
//       </section>
//     );
//   }
// }

// export default compose(
//   graphql(signup, { name: "signup" }),
//   graphql(login, { name: "login" }),
//   graphql(me),
//   withStyles(styles),
//   withRouter
// )(SignupForm);
