import gql from 'graphql-tag';

export const fetchShops = gql`
  query fetchShops {
    shops {
      id
      name
      opens
      closes
      location {
        lat
        lon
      }
      address
      name
      bottlePrice
      beers {
        id
        price
        type
        container
        size
      }
    }
  }
`;
