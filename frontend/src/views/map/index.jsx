import { graphql, compose } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { Grid, Col, Row } from 'react-flexbox-grid';
import DotsLoader from 'elements/loader/dots';
import Button from 'elements/button';

import { fetchShops } from './graphql';
import ShopOverview from '../shop/overview';

const COMPONENT = 'p-mapview';
import './style';

class MapView extends React.Component {
  static propTypes = {
    data: PropTypes.object,
    history: PropTypes.object
  };

  componentDidMount() {
    this.map = window.mapfit.MapView('mapfit', {
      theme: 'night',
      maxZoon: 14
    });
    this.map.setZoom(13);
    // set map center with coordinate
    this.map.setCenter([40.637, 22.951]);
  }

  componentDidUpdate(prevProps) {
    const { data, history } = this.props;
    if (prevProps.data.loading && !data.loading && data.shops) {
      data.shops.forEach(shop => {
        const { lat, lon } = shop.location;
        const marker = window.mapfit.Marker([lat, lon]);
        marker.on('click', () => {
          history.push(`/shop/view/${shop.id}`);
        });
        this.map.addMarker(marker);
      });
    }
  }

  render() {
    const { data, history } = this.props;
    return (
      <Grid fluid className={COMPONENT}>
        <Row>
          <Col xs={12} md={8} lg={8} sm={8}>
            <div id='mapfit' className='map' />
          </Col>
          <Col xs={12} md={4} lg={4} sm={4}>
            <div className={`${COMPONENT}__shops`}>
              {!data.loading ? (
                data.shops.map(shop => <ShopOverview shop={shop} />)
              ) : (
                <DotsLoader visible />
              )}
            </div>
            <Button
              value='Create shop'
              onClick={() => history.push('/shop/create')}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default compose(
  graphql(fetchShops),
  withRouter
)(MapView);
