import { Link } from 'react-router-dom';

export default class ShopList extends React.Component {

  static propTypes = {
    shops: PropTypes.array,
  }

  render() {
    const { shops } = this.props;
    return (
      <div>
        {shops.map(shop => (
          <div key={shop.id}>
            <Link to={`/shop/view/${shop.id}`}>{shop.id}</Link>
            <div>{shop.name}</div>
            <div>
              {shop.opens} {shop.closes}
            </div>
            <div>{shop.city}</div>
            <div>{shop.address}</div>
            <div>{shop.bottlePrice}</div>
          </div>
        ))}
      </div>
    );
  }
}
