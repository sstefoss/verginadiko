import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import loadFonts from './fonts';
import 'normalize.css';
import './css/reset';
import './css/typography';
import './css/helpers';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import client from './client';
import 'regenerator-runtime/runtime';
loadFonts();

import Map from './views/map';
import ShopCreate from './views/shop/create';
import ShopView from './views/shop/view';

ReactDOM.render(
  <ApolloProvider client={client}>
    <BrowserRouter>
      <App>
        <Switch>
          <Route path='/' exact component={Map} />
          <Route path='/shop/create' exact component={ShopCreate} />
          <Route path='/shop/:id' component={ShopView} />
        </Switch>
      </App>
    </BrowserRouter>
  </ApolloProvider>,
  document.getElementById('root')
);
registerServiceWorker();
