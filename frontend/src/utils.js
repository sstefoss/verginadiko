import { AUTH_TOKEN } from './constants';

export const isUserLoggedIn = () => {
  const token = localStorage.getItem(AUTH_TOKEN);
  return token;
};

export const saveUserToken = token => localStorage.setItem(AUTH_TOKEN, token);

export const containerToString = container => {
  if (container === 'CAN') {
    return 'Κουτάκι';
  }
  return 'Μπουκάλι';
};

export const typeToString = type => {
  if (type === 'LAGER') {
    return 'Ξανθιά';
  } else if (type === 'WEISS') {
    return 'Weiss';
  } else if (type === 'DARK') {
    return 'Μαύρη';
  } else if (type === 'PORFYRA') {
    return 'Πορφύρα';
  }
  return 'Κόκκινη';
};
