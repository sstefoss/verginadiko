import FontFaceObserver from 'fontfaceobserver';

const Fonts = () => {
  const link = document.createElement('link');
  link.href =
    'https://fonts.googleapis.com/css?family=Lato:400,400italic,900,700&amp;subset=latin';
  link.rel = 'stylesheet';

  document.head.appendChild(link);

  const roboto = new FontFaceObserver('Lato');

  roboto.load().then(() => {
    document.documentElement.classList.add('lato');
  });
};

export default Fonts;
