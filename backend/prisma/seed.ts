import { Prisma } from 'prisma-binding';

const db = new Prisma({
    secret: process.env.PRISMA_SECRET,
    endpoint: process.env.PRISMA_ENDPOINT, 
});


const setup = async () => {

}

// Remember to call the setup method in the end
setup();