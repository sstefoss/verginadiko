# Getting started

Make sure you have `yarn` and `prisma` installed globally. You will also need docker up and running in the host machine.

1. clone project and cd to folder
2. `docker-compose up -d`
2. `yarn`

# Dev

`yarn run dev` -> to start development
`prisma deploy` -> after changing the datamodel.graphql file


The project is a clone of https://github.com/prismagraphql/graphql-server-example. Check there for development tips or more instructions.
