import { Context, getUserId } from '../utils'

export const Mutation = {
  createShop(
    parent,
    { name, address, city, opens, closes, location, bottlePrice },
    ctx: Context,
    info,
  ) {
    const userId = getUserId(ctx)
    return ctx.db.mutation.createShop(
      {
        data: {
          name,
          address,
          city,
          opens,
          closes,
          bottlePrice,
          location: {
            create: {
              lat: location.lat,
              lon: location.lon,
            },
          },
          createdBy: { connect: { id: userId } },
        },
      },
      info,
    )
  },
  updateShop(parent, { id, name, address, city, opens, closes, location, bottlePrice }, ctx: Context, info) {
    const { lat, lon } = location;
    return ctx.db.mutation.updateShop(
      {
        where: { id },
        data: {
          name,
          address, 
          city,
          opens,
          closes,
          bottlePrice,
          location: {
            update: {
              lat,
              lon,
            },
          },
        },
      },
      info,
    )
  },
  deleteShop(parent, { id }, ctx: Context, info) {
    return ctx.db.mutation.deleteShop({ where: { id } }, info)
  },
  createBeer(
    parent,
    { price, type, container, size, shopId },
    ctx: Context,
    info,
  ) {
    const userId = getUserId(ctx)
    return ctx.db.mutation.createBeer(
      {
        data: {
          price,
          type,
          container,
          size,
          shop: { connect: { id: shopId } },
          createdBy: { connect: { id: userId } },
        },
      },
      info,
    )
  },
  updateBeer(parent, { id, price, type, container, size }, ctx: Context, info) {
    return ctx.db.mutation.updateBeer(
      {
        where: {
          id,
        },
        data: {
          price,
          type,
          container,
          size,
        },
      },
      info,
    )
  },
  deleteBeer(parent, { id }, ctx: Context, info) {
    return ctx.db.mutation.deleteBeer({ where: { id } }, info)
  },
}
