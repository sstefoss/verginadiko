import { Context, getUserId } from '../utils'

export const Query = {
  me(parent, _, ctx: Context, info) {
    const userId = getUserId(ctx)
    return ctx.db.query.user({ where: { id: userId } }, info)
  },
  user(parent, { id }, ctx: Context, info) {
    return ctx.db.query.user({ where: { id } }, info)
  },
  beer(parent, { id }, ctx: Context, info) {
    return ctx.db.query.beer({ where: { id } }, info)
  },
  shop(parent, { id }, ctx: Context, info) {
    return ctx.db.query.shop({ where: { id } }, info)
  },
  shops(parent, _, ctx: Context, info) {
    return ctx.db.query.shops({}, info)
  },
}
