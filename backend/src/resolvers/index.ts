import { extractFragmentReplacements } from 'prisma-binding'
import { Query } from './query'
import { Mutation } from './mutation'
import { auth } from './auth'

export const resolvers = {
  Query,
  Mutation: {
    ...auth,
    ...Mutation,
  },
}

export const fragmentReplacements = extractFragmentReplacements(resolvers)
