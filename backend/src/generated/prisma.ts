import { GraphQLResolveInfo, GraphQLSchema } from 'graphql'
import { IResolvers } from 'graphql-tools/dist/Interfaces'
import { Options } from 'graphql-binding'
import { makePrismaBindingClass, BasePrismaOptions } from 'prisma-binding'

export interface Query {
    users: <T = User[]>(args: { where?: UserWhereInput, orderBy?: UserOrderByInput, skip?: Int, after?: String, before?: String, first?: Int, last?: Int }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    shops: <T = Shop[]>(args: { where?: ShopWhereInput, orderBy?: ShopOrderByInput, skip?: Int, after?: String, before?: String, first?: Int, last?: Int }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    beers: <T = Beer[]>(args: { where?: BeerWhereInput, orderBy?: BeerOrderByInput, skip?: Int, after?: String, before?: String, first?: Int, last?: Int }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    locations: <T = Location[]>(args: { where?: LocationWhereInput, orderBy?: LocationOrderByInput, skip?: Int, after?: String, before?: String, first?: Int, last?: Int }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    user: <T = User | null>(args: { where: UserWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    shop: <T = Shop | null>(args: { where: ShopWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    beer: <T = Beer | null>(args: { where: BeerWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    usersConnection: <T = UserConnection>(args: { where?: UserWhereInput, orderBy?: UserOrderByInput, skip?: Int, after?: String, before?: String, first?: Int, last?: Int }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    shopsConnection: <T = ShopConnection>(args: { where?: ShopWhereInput, orderBy?: ShopOrderByInput, skip?: Int, after?: String, before?: String, first?: Int, last?: Int }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    beersConnection: <T = BeerConnection>(args: { where?: BeerWhereInput, orderBy?: BeerOrderByInput, skip?: Int, after?: String, before?: String, first?: Int, last?: Int }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    locationsConnection: <T = LocationConnection>(args: { where?: LocationWhereInput, orderBy?: LocationOrderByInput, skip?: Int, after?: String, before?: String, first?: Int, last?: Int }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    node: <T = Node | null>(args: { id: ID_Output }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> 
  }

export interface Mutation {
    createUser: <T = User>(args: { data: UserCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createShop: <T = Shop>(args: { data: ShopCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createBeer: <T = Beer>(args: { data: BeerCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createLocation: <T = Location>(args: { data: LocationCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateUser: <T = User | null>(args: { data: UserUpdateInput, where: UserWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateShop: <T = Shop | null>(args: { data: ShopUpdateInput, where: ShopWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateBeer: <T = Beer | null>(args: { data: BeerUpdateInput, where: BeerWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteUser: <T = User | null>(args: { where: UserWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteShop: <T = Shop | null>(args: { where: ShopWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteBeer: <T = Beer | null>(args: { where: BeerWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertUser: <T = User>(args: { where: UserWhereUniqueInput, create: UserCreateInput, update: UserUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertShop: <T = Shop>(args: { where: ShopWhereUniqueInput, create: ShopCreateInput, update: ShopUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertBeer: <T = Beer>(args: { where: BeerWhereUniqueInput, create: BeerCreateInput, update: BeerUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyUsers: <T = BatchPayload>(args: { data: UserUpdateInput, where?: UserWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyShops: <T = BatchPayload>(args: { data: ShopUpdateInput, where?: ShopWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyBeers: <T = BatchPayload>(args: { data: BeerUpdateInput, where?: BeerWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyLocations: <T = BatchPayload>(args: { data: LocationUpdateInput, where?: LocationWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyUsers: <T = BatchPayload>(args: { where?: UserWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyShops: <T = BatchPayload>(args: { where?: ShopWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyBeers: <T = BatchPayload>(args: { where?: BeerWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyLocations: <T = BatchPayload>(args: { where?: LocationWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> 
  }

export interface Subscription {
    user: <T = UserSubscriptionPayload | null>(args: { where?: UserSubscriptionWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T>> ,
    shop: <T = ShopSubscriptionPayload | null>(args: { where?: ShopSubscriptionWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T>> ,
    beer: <T = BeerSubscriptionPayload | null>(args: { where?: BeerSubscriptionWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T>> ,
    location: <T = LocationSubscriptionPayload | null>(args: { where?: LocationSubscriptionWhereInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T>> 
  }

export interface Exists {
  User: (where?: UserWhereInput) => Promise<boolean>
  Shop: (where?: ShopWhereInput) => Promise<boolean>
  Beer: (where?: BeerWhereInput) => Promise<boolean>
  Location: (where?: LocationWhereInput) => Promise<boolean>
}

export interface Prisma {
  query: Query
  mutation: Mutation
  subscription: Subscription
  exists: Exists
  request: <T = any>(query: string, variables?: {[key: string]: any}) => Promise<T>
  delegate(operation: 'query' | 'mutation', fieldName: string, args: {
    [key: string]: any;
}, infoOrQuery?: GraphQLResolveInfo | string, options?: Options): Promise<any>;
delegateSubscription(fieldName: string, args?: {
    [key: string]: any;
}, infoOrQuery?: GraphQLResolveInfo | string, options?: Options): Promise<AsyncIterator<any>>;
getAbstractResolvers(filterSchema?: GraphQLSchema | string): IResolvers;
}

export interface BindingConstructor<T> {
  new(options: BasePrismaOptions): T
}
/**
 * Type Defs
*/

const typeDefs = `enum AccessRole {
  USER
  ADMIN
}

type AggregateBeer {
  count: Int!
}

type AggregateLocation {
  count: Int!
}

type AggregateShop {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type BatchPayload {
  """The number of nodes that have been affected by the Batch operation."""
  count: Long!
}

type Beer implements Node {
  id: ID!
  price: Float!
  type: BeerType
  container: BeerContainer
  size: BeerSize
  shop(where: ShopWhereInput): Shop!
  createdBy(where: UserWhereInput): User!
  createdAt: DateTime!
}

"""A connection to a list of items."""
type BeerConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [BeerEdge]!
  aggregate: AggregateBeer!
}

enum BeerContainer {
  CAN
  BOTTLE
}

input BeerCreateInput {
  price: Float!
  type: BeerType
  container: BeerContainer
  size: BeerSize
  shop: ShopCreateOneWithoutBeersInput!
  createdBy: UserCreateOneInput!
}

input BeerCreateManyWithoutShopInput {
  create: [BeerCreateWithoutShopInput!]
  connect: [BeerWhereUniqueInput!]
}

input BeerCreateWithoutShopInput {
  price: Float!
  type: BeerType
  container: BeerContainer
  size: BeerSize
  createdBy: UserCreateOneInput!
}

"""An edge in a connection."""
type BeerEdge {
  """The item at the end of the edge."""
  node: Beer!

  """A cursor for use in pagination."""
  cursor: String!
}

enum BeerOrderByInput {
  id_ASC
  id_DESC
  price_ASC
  price_DESC
  type_ASC
  type_DESC
  container_ASC
  container_DESC
  size_ASC
  size_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type BeerPreviousValues {
  id: ID!
  price: Float!
  type: BeerType
  container: BeerContainer
  size: BeerSize
  createdAt: DateTime!
}

enum BeerSize {
  LARGE
  MEDIUM
}

type BeerSubscriptionPayload {
  mutation: MutationType!
  node: Beer
  updatedFields: [String!]
  previousValues: BeerPreviousValues
}

input BeerSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [BeerSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [BeerSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [BeerSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: BeerWhereInput
}

enum BeerType {
  LAGER
  WEISS
  DARK
  RED
  PORFYRA
}

input BeerUpdateInput {
  price: Float
  type: BeerType
  container: BeerContainer
  size: BeerSize
  shop: ShopUpdateOneWithoutBeersInput
  createdBy: UserUpdateOneInput
}

input BeerUpdateManyWithoutShopInput {
  create: [BeerCreateWithoutShopInput!]
  connect: [BeerWhereUniqueInput!]
  disconnect: [BeerWhereUniqueInput!]
  delete: [BeerWhereUniqueInput!]
  update: [BeerUpdateWithWhereUniqueWithoutShopInput!]
  upsert: [BeerUpsertWithWhereUniqueWithoutShopInput!]
}

input BeerUpdateWithoutShopDataInput {
  price: Float
  type: BeerType
  container: BeerContainer
  size: BeerSize
  createdBy: UserUpdateOneInput
}

input BeerUpdateWithWhereUniqueWithoutShopInput {
  where: BeerWhereUniqueInput!
  data: BeerUpdateWithoutShopDataInput!
}

input BeerUpsertWithWhereUniqueWithoutShopInput {
  where: BeerWhereUniqueInput!
  update: BeerUpdateWithoutShopDataInput!
  create: BeerCreateWithoutShopInput!
}

input BeerWhereInput {
  """Logical AND on all given filters."""
  AND: [BeerWhereInput!]

  """Logical OR on all given filters."""
  OR: [BeerWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [BeerWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  price: Float

  """All values that are not equal to given value."""
  price_not: Float

  """All values that are contained in given list."""
  price_in: [Float!]

  """All values that are not contained in given list."""
  price_not_in: [Float!]

  """All values less than the given value."""
  price_lt: Float

  """All values less than or equal the given value."""
  price_lte: Float

  """All values greater than the given value."""
  price_gt: Float

  """All values greater than or equal the given value."""
  price_gte: Float
  type: BeerType

  """All values that are not equal to given value."""
  type_not: BeerType

  """All values that are contained in given list."""
  type_in: [BeerType!]

  """All values that are not contained in given list."""
  type_not_in: [BeerType!]
  container: BeerContainer

  """All values that are not equal to given value."""
  container_not: BeerContainer

  """All values that are contained in given list."""
  container_in: [BeerContainer!]

  """All values that are not contained in given list."""
  container_not_in: [BeerContainer!]
  size: BeerSize

  """All values that are not equal to given value."""
  size_not: BeerSize

  """All values that are contained in given list."""
  size_in: [BeerSize!]

  """All values that are not contained in given list."""
  size_not_in: [BeerSize!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  shop: ShopWhereInput
  createdBy: UserWhereInput
}

input BeerWhereUniqueInput {
  id: ID
}

scalar DateTime

type Location {
  lat: Float!
  lon: Float!
}

"""A connection to a list of items."""
type LocationConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [LocationEdge]!
  aggregate: AggregateLocation!
}

input LocationCreateInput {
  lat: Float!
  lon: Float!
}

input LocationCreateOneInput {
  create: LocationCreateInput
}

"""An edge in a connection."""
type LocationEdge {
  """The item at the end of the edge."""
  node: Location!

  """A cursor for use in pagination."""
  cursor: String!
}

enum LocationOrderByInput {
  lat_ASC
  lat_DESC
  lon_ASC
  lon_DESC
  id_ASC
  id_DESC
  updatedAt_ASC
  updatedAt_DESC
  createdAt_ASC
  createdAt_DESC
}

type LocationPreviousValues {
  lat: Float!
  lon: Float!
}

type LocationSubscriptionPayload {
  mutation: MutationType!
  node: Location
  updatedFields: [String!]
  previousValues: LocationPreviousValues
}

input LocationSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [LocationSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [LocationSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [LocationSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: LocationWhereInput
}

input LocationUpdateDataInput {
  lat: Float
  lon: Float
}

input LocationUpdateInput {
  lat: Float
  lon: Float
}

input LocationUpdateOneInput {
  create: LocationCreateInput
  delete: Boolean
  update: LocationUpdateDataInput
  upsert: LocationUpsertNestedInput
}

input LocationUpsertNestedInput {
  update: LocationUpdateDataInput!
  create: LocationCreateInput!
}

input LocationWhereInput {
  """Logical AND on all given filters."""
  AND: [LocationWhereInput!]

  """Logical OR on all given filters."""
  OR: [LocationWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [LocationWhereInput!]
  lat: Float

  """All values that are not equal to given value."""
  lat_not: Float

  """All values that are contained in given list."""
  lat_in: [Float!]

  """All values that are not contained in given list."""
  lat_not_in: [Float!]

  """All values less than the given value."""
  lat_lt: Float

  """All values less than or equal the given value."""
  lat_lte: Float

  """All values greater than the given value."""
  lat_gt: Float

  """All values greater than or equal the given value."""
  lat_gte: Float
  lon: Float

  """All values that are not equal to given value."""
  lon_not: Float

  """All values that are contained in given list."""
  lon_in: [Float!]

  """All values that are not contained in given list."""
  lon_not_in: [Float!]

  """All values less than the given value."""
  lon_lt: Float

  """All values less than or equal the given value."""
  lon_lte: Float

  """All values greater than the given value."""
  lon_gt: Float

  """All values greater than or equal the given value."""
  lon_gte: Float
}

"""
The \`Long\` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
"""
scalar Long

type Mutation {
  createUser(data: UserCreateInput!): User!
  createShop(data: ShopCreateInput!): Shop!
  createBeer(data: BeerCreateInput!): Beer!
  createLocation(data: LocationCreateInput!): Location!
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  updateShop(data: ShopUpdateInput!, where: ShopWhereUniqueInput!): Shop
  updateBeer(data: BeerUpdateInput!, where: BeerWhereUniqueInput!): Beer
  deleteUser(where: UserWhereUniqueInput!): User
  deleteShop(where: ShopWhereUniqueInput!): Shop
  deleteBeer(where: BeerWhereUniqueInput!): Beer
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  upsertShop(where: ShopWhereUniqueInput!, create: ShopCreateInput!, update: ShopUpdateInput!): Shop!
  upsertBeer(where: BeerWhereUniqueInput!, create: BeerCreateInput!, update: BeerUpdateInput!): Beer!
  updateManyUsers(data: UserUpdateInput!, where: UserWhereInput): BatchPayload!
  updateManyShops(data: ShopUpdateInput!, where: ShopWhereInput): BatchPayload!
  updateManyBeers(data: BeerUpdateInput!, where: BeerWhereInput): BatchPayload!
  updateManyLocations(data: LocationUpdateInput!, where: LocationWhereInput): BatchPayload!
  deleteManyUsers(where: UserWhereInput): BatchPayload!
  deleteManyShops(where: ShopWhereInput): BatchPayload!
  deleteManyBeers(where: BeerWhereInput): BatchPayload!
  deleteManyLocations(where: LocationWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

"""An object with an ID"""
interface Node {
  """The id of the object."""
  id: ID!
}

"""Information about pagination in a connection."""
type PageInfo {
  """When paginating forwards, are there more items?"""
  hasNextPage: Boolean!

  """When paginating backwards, are there more items?"""
  hasPreviousPage: Boolean!

  """When paginating backwards, the cursor to continue."""
  startCursor: String

  """When paginating forwards, the cursor to continue."""
  endCursor: String
}

type Query {
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  shops(where: ShopWhereInput, orderBy: ShopOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Shop]!
  beers(where: BeerWhereInput, orderBy: BeerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Beer]!
  locations(where: LocationWhereInput, orderBy: LocationOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Location]!
  user(where: UserWhereUniqueInput!): User
  shop(where: ShopWhereUniqueInput!): Shop
  beer(where: BeerWhereUniqueInput!): Beer
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!
  shopsConnection(where: ShopWhereInput, orderBy: ShopOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): ShopConnection!
  beersConnection(where: BeerWhereInput, orderBy: BeerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): BeerConnection!
  locationsConnection(where: LocationWhereInput, orderBy: LocationOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): LocationConnection!

  """Fetches an object given its ID"""
  node(
    """The ID of an object"""
    id: ID!
  ): Node
}

type Shop implements Node {
  id: ID!
  name: String
  address: String
  city: String
  beers(where: BeerWhereInput, orderBy: BeerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Beer!]
  location(where: LocationWhereInput): Location!
  opens: String!
  closes: String!
  createdBy(where: UserWhereInput): User!
  createdAt: DateTime!
  bottlePrice: Float
}

"""A connection to a list of items."""
type ShopConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [ShopEdge]!
  aggregate: AggregateShop!
}

input ShopCreateInput {
  name: String
  address: String
  city: String
  opens: String!
  closes: String!
  bottlePrice: Float
  beers: BeerCreateManyWithoutShopInput
  location: LocationCreateOneInput!
  createdBy: UserCreateOneInput!
}

input ShopCreateOneWithoutBeersInput {
  create: ShopCreateWithoutBeersInput
  connect: ShopWhereUniqueInput
}

input ShopCreateWithoutBeersInput {
  name: String
  address: String
  city: String
  opens: String!
  closes: String!
  bottlePrice: Float
  location: LocationCreateOneInput!
  createdBy: UserCreateOneInput!
}

"""An edge in a connection."""
type ShopEdge {
  """The item at the end of the edge."""
  node: Shop!

  """A cursor for use in pagination."""
  cursor: String!
}

enum ShopOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  address_ASC
  address_DESC
  city_ASC
  city_DESC
  opens_ASC
  opens_DESC
  closes_ASC
  closes_DESC
  createdAt_ASC
  createdAt_DESC
  bottlePrice_ASC
  bottlePrice_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type ShopPreviousValues {
  id: ID!
  name: String
  address: String
  city: String
  opens: String!
  closes: String!
  createdAt: DateTime!
  bottlePrice: Float
}

type ShopSubscriptionPayload {
  mutation: MutationType!
  node: Shop
  updatedFields: [String!]
  previousValues: ShopPreviousValues
}

input ShopSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [ShopSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [ShopSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [ShopSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: ShopWhereInput
}

input ShopUpdateInput {
  name: String
  address: String
  city: String
  opens: String
  closes: String
  bottlePrice: Float
  beers: BeerUpdateManyWithoutShopInput
  location: LocationUpdateOneInput
  createdBy: UserUpdateOneInput
}

input ShopUpdateOneWithoutBeersInput {
  create: ShopCreateWithoutBeersInput
  connect: ShopWhereUniqueInput
  delete: Boolean
  update: ShopUpdateWithoutBeersDataInput
  upsert: ShopUpsertWithoutBeersInput
}

input ShopUpdateWithoutBeersDataInput {
  name: String
  address: String
  city: String
  opens: String
  closes: String
  bottlePrice: Float
  location: LocationUpdateOneInput
  createdBy: UserUpdateOneInput
}

input ShopUpsertWithoutBeersInput {
  update: ShopUpdateWithoutBeersDataInput!
  create: ShopCreateWithoutBeersInput!
}

input ShopWhereInput {
  """Logical AND on all given filters."""
  AND: [ShopWhereInput!]

  """Logical OR on all given filters."""
  OR: [ShopWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [ShopWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  address: String

  """All values that are not equal to given value."""
  address_not: String

  """All values that are contained in given list."""
  address_in: [String!]

  """All values that are not contained in given list."""
  address_not_in: [String!]

  """All values less than the given value."""
  address_lt: String

  """All values less than or equal the given value."""
  address_lte: String

  """All values greater than the given value."""
  address_gt: String

  """All values greater than or equal the given value."""
  address_gte: String

  """All values containing the given string."""
  address_contains: String

  """All values not containing the given string."""
  address_not_contains: String

  """All values starting with the given string."""
  address_starts_with: String

  """All values not starting with the given string."""
  address_not_starts_with: String

  """All values ending with the given string."""
  address_ends_with: String

  """All values not ending with the given string."""
  address_not_ends_with: String
  city: String

  """All values that are not equal to given value."""
  city_not: String

  """All values that are contained in given list."""
  city_in: [String!]

  """All values that are not contained in given list."""
  city_not_in: [String!]

  """All values less than the given value."""
  city_lt: String

  """All values less than or equal the given value."""
  city_lte: String

  """All values greater than the given value."""
  city_gt: String

  """All values greater than or equal the given value."""
  city_gte: String

  """All values containing the given string."""
  city_contains: String

  """All values not containing the given string."""
  city_not_contains: String

  """All values starting with the given string."""
  city_starts_with: String

  """All values not starting with the given string."""
  city_not_starts_with: String

  """All values ending with the given string."""
  city_ends_with: String

  """All values not ending with the given string."""
  city_not_ends_with: String
  opens: String

  """All values that are not equal to given value."""
  opens_not: String

  """All values that are contained in given list."""
  opens_in: [String!]

  """All values that are not contained in given list."""
  opens_not_in: [String!]

  """All values less than the given value."""
  opens_lt: String

  """All values less than or equal the given value."""
  opens_lte: String

  """All values greater than the given value."""
  opens_gt: String

  """All values greater than or equal the given value."""
  opens_gte: String

  """All values containing the given string."""
  opens_contains: String

  """All values not containing the given string."""
  opens_not_contains: String

  """All values starting with the given string."""
  opens_starts_with: String

  """All values not starting with the given string."""
  opens_not_starts_with: String

  """All values ending with the given string."""
  opens_ends_with: String

  """All values not ending with the given string."""
  opens_not_ends_with: String
  closes: String

  """All values that are not equal to given value."""
  closes_not: String

  """All values that are contained in given list."""
  closes_in: [String!]

  """All values that are not contained in given list."""
  closes_not_in: [String!]

  """All values less than the given value."""
  closes_lt: String

  """All values less than or equal the given value."""
  closes_lte: String

  """All values greater than the given value."""
  closes_gt: String

  """All values greater than or equal the given value."""
  closes_gte: String

  """All values containing the given string."""
  closes_contains: String

  """All values not containing the given string."""
  closes_not_contains: String

  """All values starting with the given string."""
  closes_starts_with: String

  """All values not starting with the given string."""
  closes_not_starts_with: String

  """All values ending with the given string."""
  closes_ends_with: String

  """All values not ending with the given string."""
  closes_not_ends_with: String
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  bottlePrice: Float

  """All values that are not equal to given value."""
  bottlePrice_not: Float

  """All values that are contained in given list."""
  bottlePrice_in: [Float!]

  """All values that are not contained in given list."""
  bottlePrice_not_in: [Float!]

  """All values less than the given value."""
  bottlePrice_lt: Float

  """All values less than or equal the given value."""
  bottlePrice_lte: Float

  """All values greater than the given value."""
  bottlePrice_gt: Float

  """All values greater than or equal the given value."""
  bottlePrice_gte: Float
  beers_every: BeerWhereInput
  beers_some: BeerWhereInput
  beers_none: BeerWhereInput
  location: LocationWhereInput
  createdBy: UserWhereInput
}

input ShopWhereUniqueInput {
  id: ID
}

type Subscription {
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
  shop(where: ShopSubscriptionWhereInput): ShopSubscriptionPayload
  beer(where: BeerSubscriptionWhereInput): BeerSubscriptionPayload
  location(where: LocationSubscriptionWhereInput): LocationSubscriptionPayload
}

type User implements Node {
  id: ID!
  password: String!
  email: String!
  username: String!
  accessRole: AccessRole
  createdAt: DateTime!
}

"""A connection to a list of items."""
type UserConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  password: String!
  email: String!
  username: String!
  accessRole: AccessRole
}

input UserCreateOneInput {
  create: UserCreateInput
  connect: UserWhereUniqueInput
}

"""An edge in a connection."""
type UserEdge {
  """The item at the end of the edge."""
  node: User!

  """A cursor for use in pagination."""
  cursor: String!
}

enum UserOrderByInput {
  id_ASC
  id_DESC
  password_ASC
  password_DESC
  email_ASC
  email_DESC
  username_ASC
  username_DESC
  accessRole_ASC
  accessRole_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type UserPreviousValues {
  id: ID!
  password: String!
  email: String!
  username: String!
  accessRole: AccessRole
  createdAt: DateTime!
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [UserSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: UserWhereInput
}

input UserUpdateDataInput {
  password: String
  email: String
  username: String
  accessRole: AccessRole
}

input UserUpdateInput {
  password: String
  email: String
  username: String
  accessRole: AccessRole
}

input UserUpdateOneInput {
  create: UserCreateInput
  connect: UserWhereUniqueInput
  delete: Boolean
  update: UserUpdateDataInput
  upsert: UserUpsertNestedInput
}

input UserUpsertNestedInput {
  update: UserUpdateDataInput!
  create: UserCreateInput!
}

input UserWhereInput {
  """Logical AND on all given filters."""
  AND: [UserWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  password: String

  """All values that are not equal to given value."""
  password_not: String

  """All values that are contained in given list."""
  password_in: [String!]

  """All values that are not contained in given list."""
  password_not_in: [String!]

  """All values less than the given value."""
  password_lt: String

  """All values less than or equal the given value."""
  password_lte: String

  """All values greater than the given value."""
  password_gt: String

  """All values greater than or equal the given value."""
  password_gte: String

  """All values containing the given string."""
  password_contains: String

  """All values not containing the given string."""
  password_not_contains: String

  """All values starting with the given string."""
  password_starts_with: String

  """All values not starting with the given string."""
  password_not_starts_with: String

  """All values ending with the given string."""
  password_ends_with: String

  """All values not ending with the given string."""
  password_not_ends_with: String
  email: String

  """All values that are not equal to given value."""
  email_not: String

  """All values that are contained in given list."""
  email_in: [String!]

  """All values that are not contained in given list."""
  email_not_in: [String!]

  """All values less than the given value."""
  email_lt: String

  """All values less than or equal the given value."""
  email_lte: String

  """All values greater than the given value."""
  email_gt: String

  """All values greater than or equal the given value."""
  email_gte: String

  """All values containing the given string."""
  email_contains: String

  """All values not containing the given string."""
  email_not_contains: String

  """All values starting with the given string."""
  email_starts_with: String

  """All values not starting with the given string."""
  email_not_starts_with: String

  """All values ending with the given string."""
  email_ends_with: String

  """All values not ending with the given string."""
  email_not_ends_with: String
  username: String

  """All values that are not equal to given value."""
  username_not: String

  """All values that are contained in given list."""
  username_in: [String!]

  """All values that are not contained in given list."""
  username_not_in: [String!]

  """All values less than the given value."""
  username_lt: String

  """All values less than or equal the given value."""
  username_lte: String

  """All values greater than the given value."""
  username_gt: String

  """All values greater than or equal the given value."""
  username_gte: String

  """All values containing the given string."""
  username_contains: String

  """All values not containing the given string."""
  username_not_contains: String

  """All values starting with the given string."""
  username_starts_with: String

  """All values not starting with the given string."""
  username_not_starts_with: String

  """All values ending with the given string."""
  username_ends_with: String

  """All values not ending with the given string."""
  username_not_ends_with: String
  accessRole: AccessRole

  """All values that are not equal to given value."""
  accessRole_not: AccessRole

  """All values that are contained in given list."""
  accessRole_in: [AccessRole!]

  """All values that are not contained in given list."""
  accessRole_not_in: [AccessRole!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
}

input UserWhereUniqueInput {
  id: ID
  email: String
}
`

export const Prisma = makePrismaBindingClass<BindingConstructor<Prisma>>({typeDefs})

/**
 * Types
*/

export type BeerType =   'LAGER' |
  'WEISS' |
  'DARK' |
  'RED' |
  'PORFYRA'

export type BeerContainer =   'CAN' |
  'BOTTLE'

export type BeerSize =   'LARGE' |
  'MEDIUM'

export type ShopOrderByInput =   'id_ASC' |
  'id_DESC' |
  'name_ASC' |
  'name_DESC' |
  'address_ASC' |
  'address_DESC' |
  'city_ASC' |
  'city_DESC' |
  'opens_ASC' |
  'opens_DESC' |
  'closes_ASC' |
  'closes_DESC' |
  'createdAt_ASC' |
  'createdAt_DESC' |
  'bottlePrice_ASC' |
  'bottlePrice_DESC' |
  'updatedAt_ASC' |
  'updatedAt_DESC'

export type BeerOrderByInput =   'id_ASC' |
  'id_DESC' |
  'price_ASC' |
  'price_DESC' |
  'type_ASC' |
  'type_DESC' |
  'container_ASC' |
  'container_DESC' |
  'size_ASC' |
  'size_DESC' |
  'createdAt_ASC' |
  'createdAt_DESC' |
  'updatedAt_ASC' |
  'updatedAt_DESC'

export type UserOrderByInput =   'id_ASC' |
  'id_DESC' |
  'password_ASC' |
  'password_DESC' |
  'email_ASC' |
  'email_DESC' |
  'username_ASC' |
  'username_DESC' |
  'accessRole_ASC' |
  'accessRole_DESC' |
  'createdAt_ASC' |
  'createdAt_DESC' |
  'updatedAt_ASC' |
  'updatedAt_DESC'

export type LocationOrderByInput =   'lat_ASC' |
  'lat_DESC' |
  'lon_ASC' |
  'lon_DESC' |
  'id_ASC' |
  'id_DESC' |
  'updatedAt_ASC' |
  'updatedAt_DESC' |
  'createdAt_ASC' |
  'createdAt_DESC'

export type AccessRole =   'USER' |
  'ADMIN'

export type MutationType =   'CREATED' |
  'UPDATED' |
  'DELETED'

export interface ShopCreateInput {
  name?: String
  address?: String
  city?: String
  opens: String
  closes: String
  bottlePrice?: Float
  beers?: BeerCreateManyWithoutShopInput
  location: LocationCreateOneInput
  createdBy: UserCreateOneInput
}

export interface UserWhereInput {
  AND?: UserWhereInput[] | UserWhereInput
  OR?: UserWhereInput[] | UserWhereInput
  NOT?: UserWhereInput[] | UserWhereInput
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  password?: String
  password_not?: String
  password_in?: String[] | String
  password_not_in?: String[] | String
  password_lt?: String
  password_lte?: String
  password_gt?: String
  password_gte?: String
  password_contains?: String
  password_not_contains?: String
  password_starts_with?: String
  password_not_starts_with?: String
  password_ends_with?: String
  password_not_ends_with?: String
  email?: String
  email_not?: String
  email_in?: String[] | String
  email_not_in?: String[] | String
  email_lt?: String
  email_lte?: String
  email_gt?: String
  email_gte?: String
  email_contains?: String
  email_not_contains?: String
  email_starts_with?: String
  email_not_starts_with?: String
  email_ends_with?: String
  email_not_ends_with?: String
  username?: String
  username_not?: String
  username_in?: String[] | String
  username_not_in?: String[] | String
  username_lt?: String
  username_lte?: String
  username_gt?: String
  username_gte?: String
  username_contains?: String
  username_not_contains?: String
  username_starts_with?: String
  username_not_starts_with?: String
  username_ends_with?: String
  username_not_ends_with?: String
  accessRole?: AccessRole
  accessRole_not?: AccessRole
  accessRole_in?: AccessRole[] | AccessRole
  accessRole_not_in?: AccessRole[] | AccessRole
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
}

export interface BeerCreateInput {
  price: Float
  type?: BeerType
  container?: BeerContainer
  size?: BeerSize
  shop: ShopCreateOneWithoutBeersInput
  createdBy: UserCreateOneInput
}

export interface BeerWhereInput {
  AND?: BeerWhereInput[] | BeerWhereInput
  OR?: BeerWhereInput[] | BeerWhereInput
  NOT?: BeerWhereInput[] | BeerWhereInput
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  price?: Float
  price_not?: Float
  price_in?: Float[] | Float
  price_not_in?: Float[] | Float
  price_lt?: Float
  price_lte?: Float
  price_gt?: Float
  price_gte?: Float
  type?: BeerType
  type_not?: BeerType
  type_in?: BeerType[] | BeerType
  type_not_in?: BeerType[] | BeerType
  container?: BeerContainer
  container_not?: BeerContainer
  container_in?: BeerContainer[] | BeerContainer
  container_not_in?: BeerContainer[] | BeerContainer
  size?: BeerSize
  size_not?: BeerSize
  size_in?: BeerSize[] | BeerSize
  size_not_in?: BeerSize[] | BeerSize
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  shop?: ShopWhereInput
  createdBy?: UserWhereInput
}

export interface ShopCreateOneWithoutBeersInput {
  create?: ShopCreateWithoutBeersInput
  connect?: ShopWhereUniqueInput
}

export interface ShopWhereInput {
  AND?: ShopWhereInput[] | ShopWhereInput
  OR?: ShopWhereInput[] | ShopWhereInput
  NOT?: ShopWhereInput[] | ShopWhereInput
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  name?: String
  name_not?: String
  name_in?: String[] | String
  name_not_in?: String[] | String
  name_lt?: String
  name_lte?: String
  name_gt?: String
  name_gte?: String
  name_contains?: String
  name_not_contains?: String
  name_starts_with?: String
  name_not_starts_with?: String
  name_ends_with?: String
  name_not_ends_with?: String
  address?: String
  address_not?: String
  address_in?: String[] | String
  address_not_in?: String[] | String
  address_lt?: String
  address_lte?: String
  address_gt?: String
  address_gte?: String
  address_contains?: String
  address_not_contains?: String
  address_starts_with?: String
  address_not_starts_with?: String
  address_ends_with?: String
  address_not_ends_with?: String
  city?: String
  city_not?: String
  city_in?: String[] | String
  city_not_in?: String[] | String
  city_lt?: String
  city_lte?: String
  city_gt?: String
  city_gte?: String
  city_contains?: String
  city_not_contains?: String
  city_starts_with?: String
  city_not_starts_with?: String
  city_ends_with?: String
  city_not_ends_with?: String
  opens?: String
  opens_not?: String
  opens_in?: String[] | String
  opens_not_in?: String[] | String
  opens_lt?: String
  opens_lte?: String
  opens_gt?: String
  opens_gte?: String
  opens_contains?: String
  opens_not_contains?: String
  opens_starts_with?: String
  opens_not_starts_with?: String
  opens_ends_with?: String
  opens_not_ends_with?: String
  closes?: String
  closes_not?: String
  closes_in?: String[] | String
  closes_not_in?: String[] | String
  closes_lt?: String
  closes_lte?: String
  closes_gt?: String
  closes_gte?: String
  closes_contains?: String
  closes_not_contains?: String
  closes_starts_with?: String
  closes_not_starts_with?: String
  closes_ends_with?: String
  closes_not_ends_with?: String
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  bottlePrice?: Float
  bottlePrice_not?: Float
  bottlePrice_in?: Float[] | Float
  bottlePrice_not_in?: Float[] | Float
  bottlePrice_lt?: Float
  bottlePrice_lte?: Float
  bottlePrice_gt?: Float
  bottlePrice_gte?: Float
  beers_every?: BeerWhereInput
  beers_some?: BeerWhereInput
  beers_none?: BeerWhereInput
  location?: LocationWhereInput
  createdBy?: UserWhereInput
}

export interface ShopCreateWithoutBeersInput {
  name?: String
  address?: String
  city?: String
  opens: String
  closes: String
  bottlePrice?: Float
  location: LocationCreateOneInput
  createdBy: UserCreateOneInput
}

export interface LocationWhereInput {
  AND?: LocationWhereInput[] | LocationWhereInput
  OR?: LocationWhereInput[] | LocationWhereInput
  NOT?: LocationWhereInput[] | LocationWhereInput
  lat?: Float
  lat_not?: Float
  lat_in?: Float[] | Float
  lat_not_in?: Float[] | Float
  lat_lt?: Float
  lat_lte?: Float
  lat_gt?: Float
  lat_gte?: Float
  lon?: Float
  lon_not?: Float
  lon_in?: Float[] | Float
  lon_not_in?: Float[] | Float
  lon_lt?: Float
  lon_lte?: Float
  lon_gt?: Float
  lon_gte?: Float
}

export interface LocationUpdateOneInput {
  create?: LocationCreateInput
  delete?: Boolean
  update?: LocationUpdateDataInput
  upsert?: LocationUpsertNestedInput
}

export interface ShopUpdateInput {
  name?: String
  address?: String
  city?: String
  opens?: String
  closes?: String
  bottlePrice?: Float
  beers?: BeerUpdateManyWithoutShopInput
  location?: LocationUpdateOneInput
  createdBy?: UserUpdateOneInput
}

export interface BeerUpsertWithWhereUniqueWithoutShopInput {
  where: BeerWhereUniqueInput
  update: BeerUpdateWithoutShopDataInput
  create: BeerCreateWithoutShopInput
}

export interface UserUpdateInput {
  password?: String
  email?: String
  username?: String
  accessRole?: AccessRole
}

export interface UserUpsertNestedInput {
  update: UserUpdateDataInput
  create: UserCreateInput
}

export interface BeerSubscriptionWhereInput {
  AND?: BeerSubscriptionWhereInput[] | BeerSubscriptionWhereInput
  OR?: BeerSubscriptionWhereInput[] | BeerSubscriptionWhereInput
  NOT?: BeerSubscriptionWhereInput[] | BeerSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: BeerWhereInput
}

export interface UserUpdateDataInput {
  password?: String
  email?: String
  username?: String
  accessRole?: AccessRole
}

export interface UserWhereUniqueInput {
  id?: ID_Input
  email?: String
}

export interface UserUpdateOneInput {
  create?: UserCreateInput
  connect?: UserWhereUniqueInput
  delete?: Boolean
  update?: UserUpdateDataInput
  upsert?: UserUpsertNestedInput
}

export interface BeerWhereUniqueInput {
  id?: ID_Input
}

export interface BeerUpdateWithoutShopDataInput {
  price?: Float
  type?: BeerType
  container?: BeerContainer
  size?: BeerSize
  createdBy?: UserUpdateOneInput
}

export interface LocationUpdateInput {
  lat?: Float
  lon?: Float
}

export interface UserCreateInput {
  password: String
  email: String
  username: String
  accessRole?: AccessRole
}

export interface ShopUpdateWithoutBeersDataInput {
  name?: String
  address?: String
  city?: String
  opens?: String
  closes?: String
  bottlePrice?: Float
  location?: LocationUpdateOneInput
  createdBy?: UserUpdateOneInput
}

export interface BeerUpdateWithWhereUniqueWithoutShopInput {
  where: BeerWhereUniqueInput
  data: BeerUpdateWithoutShopDataInput
}

export interface BeerUpdateInput {
  price?: Float
  type?: BeerType
  container?: BeerContainer
  size?: BeerSize
  shop?: ShopUpdateOneWithoutBeersInput
  createdBy?: UserUpdateOneInput
}

export interface BeerCreateManyWithoutShopInput {
  create?: BeerCreateWithoutShopInput[] | BeerCreateWithoutShopInput
  connect?: BeerWhereUniqueInput[] | BeerWhereUniqueInput
}

export interface LocationUpdateDataInput {
  lat?: Float
  lon?: Float
}

export interface BeerCreateWithoutShopInput {
  price: Float
  type?: BeerType
  container?: BeerContainer
  size?: BeerSize
  createdBy: UserCreateOneInput
}

export interface ShopSubscriptionWhereInput {
  AND?: ShopSubscriptionWhereInput[] | ShopSubscriptionWhereInput
  OR?: ShopSubscriptionWhereInput[] | ShopSubscriptionWhereInput
  NOT?: ShopSubscriptionWhereInput[] | ShopSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: ShopWhereInput
}

export interface UserCreateOneInput {
  create?: UserCreateInput
  connect?: UserWhereUniqueInput
}

export interface UserSubscriptionWhereInput {
  AND?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput
  OR?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput
  NOT?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: UserWhereInput
}

export interface ShopUpdateOneWithoutBeersInput {
  create?: ShopCreateWithoutBeersInput
  connect?: ShopWhereUniqueInput
  delete?: Boolean
  update?: ShopUpdateWithoutBeersDataInput
  upsert?: ShopUpsertWithoutBeersInput
}

export interface BeerUpdateManyWithoutShopInput {
  create?: BeerCreateWithoutShopInput[] | BeerCreateWithoutShopInput
  connect?: BeerWhereUniqueInput[] | BeerWhereUniqueInput
  disconnect?: BeerWhereUniqueInput[] | BeerWhereUniqueInput
  delete?: BeerWhereUniqueInput[] | BeerWhereUniqueInput
  update?: BeerUpdateWithWhereUniqueWithoutShopInput[] | BeerUpdateWithWhereUniqueWithoutShopInput
  upsert?: BeerUpsertWithWhereUniqueWithoutShopInput[] | BeerUpsertWithWhereUniqueWithoutShopInput
}

export interface LocationCreateInput {
  lat: Float
  lon: Float
}

export interface LocationCreateOneInput {
  create?: LocationCreateInput
}

export interface LocationUpsertNestedInput {
  update: LocationUpdateDataInput
  create: LocationCreateInput
}

export interface ShopUpsertWithoutBeersInput {
  update: ShopUpdateWithoutBeersDataInput
  create: ShopCreateWithoutBeersInput
}

export interface ShopWhereUniqueInput {
  id?: ID_Input
}

export interface LocationSubscriptionWhereInput {
  AND?: LocationSubscriptionWhereInput[] | LocationSubscriptionWhereInput
  OR?: LocationSubscriptionWhereInput[] | LocationSubscriptionWhereInput
  NOT?: LocationSubscriptionWhereInput[] | LocationSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: LocationWhereInput
}

/*
 * An object with an ID

 */
export interface Node {
  id: ID_Output
}

export interface LocationPreviousValues {
  lat: Float
  lon: Float
}

export interface BatchPayload {
  count: Long
}

export interface Shop extends Node {
  id: ID_Output
  name?: String
  address?: String
  city?: String
  beers?: Beer[]
  location: Location
  opens: String
  closes: String
  createdBy: User
  createdAt: DateTime
  bottlePrice?: Float
}

export interface LocationSubscriptionPayload {
  mutation: MutationType
  node?: Location
  updatedFields?: String[]
  previousValues?: LocationPreviousValues
}

export interface Location {
  lat: Float
  lon: Float
}

export interface User extends Node {
  id: ID_Output
  password: String
  email: String
  username: String
  accessRole?: AccessRole
  createdAt: DateTime
}

/*
 * An edge in a connection.

 */
export interface LocationEdge {
  node: Location
  cursor: String
}

export interface Beer extends Node {
  id: ID_Output
  price: Float
  type?: BeerType
  container?: BeerContainer
  size?: BeerSize
  shop: Shop
  createdBy: User
  createdAt: DateTime
}

export interface AggregateBeer {
  count: Int
}

/*
 * Information about pagination in a connection.

 */
export interface PageInfo {
  hasNextPage: Boolean
  hasPreviousPage: Boolean
  startCursor?: String
  endCursor?: String
}

/*
 * A connection to a list of items.

 */
export interface BeerConnection {
  pageInfo: PageInfo
  edges: BeerEdge[]
  aggregate: AggregateBeer
}

/*
 * A connection to a list of items.

 */
export interface UserConnection {
  pageInfo: PageInfo
  edges: UserEdge[]
  aggregate: AggregateUser
}

/*
 * An edge in a connection.

 */
export interface ShopEdge {
  node: Shop
  cursor: String
}

export interface BeerPreviousValues {
  id: ID_Output
  price: Float
  type?: BeerType
  container?: BeerContainer
  size?: BeerSize
  createdAt: DateTime
}

export interface AggregateUser {
  count: Int
}

export interface UserSubscriptionPayload {
  mutation: MutationType
  node?: User
  updatedFields?: String[]
  previousValues?: UserPreviousValues
}

export interface AggregateLocation {
  count: Int
}

export interface ShopPreviousValues {
  id: ID_Output
  name?: String
  address?: String
  city?: String
  opens: String
  closes: String
  createdAt: DateTime
  bottlePrice?: Float
}

export interface ShopSubscriptionPayload {
  mutation: MutationType
  node?: Shop
  updatedFields?: String[]
  previousValues?: ShopPreviousValues
}

export interface BeerSubscriptionPayload {
  mutation: MutationType
  node?: Beer
  updatedFields?: String[]
  previousValues?: BeerPreviousValues
}

export interface UserPreviousValues {
  id: ID_Output
  password: String
  email: String
  username: String
  accessRole?: AccessRole
  createdAt: DateTime
}

/*
 * A connection to a list of items.

 */
export interface LocationConnection {
  pageInfo: PageInfo
  edges: LocationEdge[]
  aggregate: AggregateLocation
}

/*
 * An edge in a connection.

 */
export interface UserEdge {
  node: User
  cursor: String
}

/*
 * A connection to a list of items.

 */
export interface ShopConnection {
  pageInfo: PageInfo
  edges: ShopEdge[]
  aggregate: AggregateShop
}

export interface AggregateShop {
  count: Int
}

/*
 * An edge in a connection.

 */
export interface BeerEdge {
  node: Beer
  cursor: String
}

/*
The `Long` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
*/
export type Long = string

/*
The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1. 
*/
export type Int = number

/*
The `Float` scalar type represents signed double-precision fractional values as specified by [IEEE 754](http://en.wikipedia.org/wiki/IEEE_floating_point). 
*/
export type Float = number

/*
The `Boolean` scalar type represents `true` or `false`.
*/
export type Boolean = boolean

/*
The `ID` scalar type represents a unique identifier, often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as an ID.
*/
export type ID_Input = string | number
export type ID_Output = string

/*
The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.
*/
export type String = string

export type DateTime = Date | string